package com.koandr.organizer;

import java.io.*;
import javax.xml.parsers.*;
import java.util.*;

import android.util.*;
import android.content.*;

import org.w3c.dom.*;
import org.xml.sax.*;
import org.xmlpull.v1.*;

public class NewsRSSStorage {
	
	public void setBookmarks(Context th, ArrayList<NewsRSSBookmark> bookmarks) {	
		String stringXML = null;
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		try {
			serializer.setOutput(writer);
			serializer.startDocument("UTF-8", true);
			serializer.startTag("", "bookmarks");
			for (NewsRSSBookmark bookmark: bookmarks) {
				serializer.startTag("", "bookmark");
				serializer.attribute("", "title", bookmark.getTitle());
				serializer.attribute("", "url", bookmark.getURL());
				serializer.endTag("", "bookmark");
			}
			serializer.endTag("", "bookmarks");
			serializer.endDocument();
			stringXML = writer.toString();
		} catch (Exception e) {
			
		} 	
		try {
			OutputStream outputstream = th.openFileOutput("NewsRSSBookmarks.xml", 0);
			OutputStreamWriter osw = new OutputStreamWriter(outputstream);		
			osw.write(stringXML);
			osw.close();
		} catch (Throwable t) {
			
		}
	}
	
	public ArrayList<NewsRSSBookmark> getBookmarks(Context th) {
		ArrayList<NewsRSSBookmark> bookmarks = new ArrayList<NewsRSSBookmark>();
		String ms = "";
		try {
			InputStream inputstream = th.openFileInput("NewsRSSBookmarks.xml");
			if (inputstream != null) {
				InputStreamReader isr = new InputStreamReader(inputstream);
				BufferedReader reader = new BufferedReader(isr);
				String str;
				StringBuffer buffer = new StringBuffer();
				while ((str = reader.readLine()) != null) {
					buffer.append(str + "\n");
					ms = ms+str;
				}
				inputstream.close();
			}
		} catch (Throwable t) {
			
		}	
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(ms)));
			doc.getDocumentElement().normalize();
			NodeList list = doc.getElementsByTagName("bookmark");
			for (int i = 0; i < list.getLength(); i++) {
				NewsRSSBookmark bookmark = new NewsRSSBookmark();
				bookmark.setTitle(list.item(i).getAttributes().getNamedItem("title").getNodeValue());
				bookmark.setURL(list.item(i).getAttributes().getNamedItem("url").getNodeValue());
				bookmarks.add(bookmark);
			}
		} catch (Exception e) {
			
		} 
		return bookmarks;
	}
	
	@SuppressWarnings("deprecation")
	public void setCardsCash(Context th, ArrayList<NewsRSSCard> cards) {	
		String stringXML = null;
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		try {
			serializer.setOutput(writer);
			serializer.startDocument("UTF-8", true);
			serializer.startTag("", "cards");
			for (NewsRSSCard card: cards) {
				serializer.startTag("", "card");
				serializer.attribute("", "siteTitle", card.getSiteTitle());
				serializer.attribute("", "title", card.getTitle());
				serializer.attribute("", "text", card.getText());
				serializer.attribute("", "url", card.getURL());
				serializer.attribute("", "date", card.getDate());
				serializer.endTag("", "card");
			}
			serializer.endTag("", "cards");
			serializer.endDocument();
			stringXML = writer.toString();
		} catch (Exception e) {
			
		} 	
		try {
			OutputStream outputstream = th.openFileOutput("NewsRSSCardsCash.xml", 0);
			OutputStreamWriter osw = new OutputStreamWriter(outputstream);		
			osw.write(stringXML);
			osw.close();
		} catch (Throwable t) {
			
		}
	}
	
	public ArrayList<NewsRSSCard> getCardsCash(Context th) {
		ArrayList<NewsRSSCard> cards = new ArrayList<NewsRSSCard>();
		String ms = "";
		try {
			InputStream inputstream = th.openFileInput("NewsRSSCardsCash.xml");
			if (inputstream != null) {
				InputStreamReader isr = new InputStreamReader(inputstream);
				BufferedReader reader = new BufferedReader(isr);
				String str;
				StringBuffer buffer = new StringBuffer();
				while ((str = reader.readLine()) != null) {
					buffer.append(str + "\n");
					ms = ms+str;
				}
				inputstream.close();
			}
		} catch (Throwable t) {
			
		}	
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(ms)));
			doc.getDocumentElement().normalize();
			NodeList list = doc.getElementsByTagName("card");
			for (int i = 0; i < list.getLength(); i++) {
				NewsRSSCard card = new NewsRSSCard();
				card.setSiteTitle(list.item(i).getAttributes().getNamedItem("siteTitle").getNodeValue());
				card.setTitle(list.item(i).getAttributes().getNamedItem("title").getNodeValue());
				card.setText(list.item(i).getAttributes().getNamedItem("text").getNodeValue());
				card.setURL(list.item(i).getAttributes().getNamedItem("url").getNodeValue());
				card.setDate(list.item(i).getAttributes().getNamedItem("date").getNodeValue());
				cards.add(card);
			}
		} catch (Exception e) {
			
		} 
		return cards;
	}
	
}
