package com.koandr.organizer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class TextEditorDialogFileBrowser extends DialogFragment implements OnClickListener {

	public Uri data;
	public final static String THIEF = "com.koandr.organizer.THIEF";
	private List<String> df = new ArrayList<String>();
	private List<String> directoryEntries = new ArrayList<String>();
	private File currentDirectory = new File("/");
	TextView titleManager;
	ListView listView;
	
	public static TextEditorDialogFileBrowser newInstance(int num) {
		TextEditorDialogFileBrowser dialogFragment = new TextEditorDialogFileBrowser();
	    Bundle bundle = new Bundle();
	    bundle.putInt("num", num);
	    dialogFragment.setArguments(bundle);
	    return dialogFragment;
    }

	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View rootView = inflater.inflate(R.layout.texteditor_filebrowser, null);
		titleManager = (TextView) rootView.findViewById(R.id.titleManager);
		listView = (ListView) rootView.findViewById(R.id.texteditor_filebrowser_list);
		Bundle bundle = getArguments();
		String itemname = bundle.getString("dssID");
		if (itemname.length() != 0) {
            browseTo((new File(itemname)).getParentFile());
        } else {
            browseTo(new File("/mnt"));
        }
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
	    .setTitle(R.string.open_file)
	    .setView(rootView)
	    .setNeutralButton(R.string.cancel, this)
		.setCancelable(false);
		return adb.create();
	}
	
    private void upOneLevel(){
        if(this.currentDirectory.getParent() != null)
            this.browseTo(this.currentDirectory.getParentFile());
    }

    private void browseTo(final File aDirectory){
        if (aDirectory.isDirectory()) {
            if (aDirectory.canRead()) {
                this.currentDirectory = aDirectory;
                fill(aDirectory.listFiles());
                titleManager.setText(aDirectory.getAbsolutePath());
            }
        } else {
            OnClickListener okButtonListener = new OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    String	uri = (aDirectory.getAbsolutePath());
                    Intent i = new Intent();
                    i.putExtra(THIEF, uri);
                    int INT_CODE = 5;//1;
                    getTargetFragment().onActivityResult(getTargetRequestCode(), INT_CODE, i);
                    dismiss();
                }
            };
            OnClickListener cancelButtonListener = new OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {

                }

            };
            new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.open_file))
                .setMessage(getString(R.string.are_you_sure))
                .setPositiveButton(getString(R.string.ok), okButtonListener)
                .setNegativeButton(getString(R.string.no), cancelButtonListener)
                .show();
        }

    }

    private void fill(File[] files) {
        this.directoryEntries.clear();
        this.df.clear();

        if (this.currentDirectory.getParent() != null) {
            this.directoryEntries.add("..");
            this.df.add("..");
        }
        for (File file : files) {
            this.directoryEntries.add(file.getAbsolutePath());
            this.df.add(file.getName());
        }
        ArrayAdapter<String> directoryList = new ArrayAdapter<String>(getActivity(), R.layout.texteditor_filebrowser_item, //this.
                df);//R.id.label,  this.directoryEntries);

        listView.setAdapter(directoryList);
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3) {
                int selectionRowID = arg2;
                String selectedFileString = directoryEntries.get(selectionRowID);
                if (selectedFileString.equals("..")) {
                    upOneLevel();
                } else {
                    File clickedFile = null;
                    clickedFile = new File(selectedFileString);
                    if (clickedFile != null) {
                        browseTo(clickedFile);
                    }
                }

            }
        });

    }

    public static String getExtension(String uri) {
        if (uri == null) {
            return null;
        }
        int dot = uri.lastIndexOf(".");
        if (dot >= 0) {
            return uri.substring(dot);
        } else {
            // No extension.
            return "";
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub

    }

}
