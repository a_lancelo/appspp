package com.koandr.organizer;

import android.content.Context;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by Andrew on 9/26/14.
 */
public class NotesStorage {

    public void setCards(Context th, ArrayList<NotesCard> cards) {
        String stringXML = null;
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "cards");
            for (NotesCard card: cards) {
                serializer.startTag("", "card");
                serializer.attribute("", "text", card.getText());
                serializer.attribute("", "date", card.getDate());
                serializer.endTag("", "card");
            }
            serializer.endTag("", "cards");
            serializer.endDocument();
            stringXML = writer.toString();
        } catch (Exception e) {

        }
        try {
            OutputStream outputstream = th.openFileOutput("NotesCards.xml", 0);
            OutputStreamWriter osw = new OutputStreamWriter(outputstream);
            osw.write(stringXML);
            osw.close();
        } catch (Throwable t) {

        }
    }

    public ArrayList<NotesCard> getCards(Context th) {
        ArrayList<NotesCard> cards = new ArrayList<NotesCard>();
        String ms = "";
        try {
            InputStream inputstream = th.openFileInput("NotesCards.xml");
            if (inputstream != null) {
                InputStreamReader isr = new InputStreamReader(inputstream);
                BufferedReader reader = new BufferedReader(isr);
                String str;
                StringBuffer buffer = new StringBuffer();
                while ((str = reader.readLine()) != null) {
                    buffer.append(str + "\n");
                    ms = ms+str;
                }
                inputstream.close();
            }
        } catch (Throwable t) {

        }
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(ms)));
            doc.getDocumentElement().normalize();
            NodeList list = doc.getElementsByTagName("card");
            for (int i = 0; i < list.getLength(); i++) {
                NotesCard card = new NotesCard();
                card.setText(list.item(i).getAttributes().getNamedItem("text").getNodeValue());
                card.setDate(list.item(i).getAttributes().getNamedItem("date").getNodeValue());
                cards.add(card);
            }
        } catch (Exception e) {

        }
        return cards;
    }

}
