package com.koandr.organizer;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
//import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class NewsRSSBookmarksFragment extends Fragment {
	
	private ArrayList<NewsRSSBookmark> bookmarks;
	private ListView listView;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getActivity().setTitle(R.string.news_rss);
		//final
		bookmarks = new NewsRSSStorage().getBookmarks(getActivity());	
        View rootView = inflater.inflate(R.layout.news_rss_bookmarks, container, false);
        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.setAdapter(new NewsRSSBookmarksAdapter(getActivity(), bookmarks)); //names.toArray(new String[names.size()]), artists.toArray(new String[artists.size()])));				
        listView.setOnItemClickListener(itemListener);
        listView.setOnItemLongClickListener(itemLongListener);
        EditText editText = (EditText) rootView.findViewById(R.id.editText);
        editText.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN & keyCode == KeyEvent.KEYCODE_ENTER) {
					NewsRSSBookmark bookmark = new NewsRSSBookmark();
					bookmark.setTitle(getString(R.string.title_loading));
					bookmark.setURL(((EditText) v).getText().toString());
					bookmarks.add(bookmark);
					new NewsRSSStorage().setBookmarks(getActivity(), bookmarks);
					bookmarks = new NewsRSSStorage().getBookmarks(getActivity());	
					((EditText) v).setText("");
			        listView.setAdapter(new NewsRSSBookmarksAdapter(getActivity(), bookmarks)); 
					return true;
				}
				return false;
			}
			
		});
        setHasOptionsMenu(true);
        return rootView;		
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.news_rss_bookmarks, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_view_as_list) {
			setResult(-1);
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == AppConst.NewsRSSBookmarksEditor) {
			int position = data.getExtras().getInt("position");
			switch (resultCode) {
				case 1:
					bookmarks.get(position).setURL(data.getExtras().getString("url"));
					break;
				case 2:
					bookmarks.remove(position);
					break;
			}
			new NewsRSSStorage().setBookmarks(getActivity(), bookmarks);
			bookmarks = new NewsRSSStorage().getBookmarks(getActivity());	
	        listView.setAdapter(new NewsRSSBookmarksAdapter(getActivity(), bookmarks)); 
		}
	}
	
	OnItemClickListener itemListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View v,
								int position, long id) { 
			setResult(position);
		}
	};
	
	OnItemLongClickListener itemLongListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View v,
								int position, long id) { 
			// put pos
			DialogFragment editBookmark = new NewsRSSBookmarksEditorDialog();
			Bundle bundle = new Bundle();
			bundle.putString("title", bookmarks.get(position).getTitle());
			bundle.putString("url", bookmarks.get(position).getURL());
			bundle.putInt("position", position);
			editBookmark.setArguments(bundle);
			editBookmark.setTargetFragment(NewsRSSBookmarksFragment.this, AppConst.NewsRSSBookmarksEditor);
			editBookmark.show(getFragmentManager(), "dialog");
			return true;
		}
	};
	
	private void setResult(int position) {
		Editor editor = getActivity().getSharedPreferences(AppConst.PreferencesFile, Activity.MODE_PRIVATE).edit();
		editor.putInt("newsPosition", position);
		editor.commit();
		Intent data = new Intent();
		data.putExtra("position", position);
		getActivity().setResult(1, data);
		getActivity().finish();
	}
	
	public class NewsRSSBookmarksAdapter extends BaseAdapter {
		
		Context context;
		//String [] names, artists;
		ArrayList<NewsRSSBookmark> bookmarks;
		
		NewsRSSBookmarksAdapter(Context context, ArrayList<NewsRSSBookmark> bookmarks) {
	       this.context = context;
	       this.bookmarks = bookmarks;
	       //this.names = names;
	       //this.artists = artists;
	    }
		
		@Override
		public int getCount() {
			return bookmarks.size();
		}
		
		@Override
		public Object getItem(int position) {
		   return null;
		}
		
		@Override
		public long getItemId(int position) {
		   return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
		   View view = (View) ((AppMainActivity) context).getLayoutInflater().inflate(R.layout.music_player_item, parent, false); 
		   ((TextView) view.findViewById(R.id.name)).setText(bookmarks.get(position).getTitle());//	names[position]);
		   ((TextView) view.findViewById(R.id.artist)).setText(bookmarks.get(position).getURL());
		   return view;
		}
		
	}
	
}
