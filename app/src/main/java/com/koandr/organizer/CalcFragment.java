package com.koandr.organizer;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class CalcFragment extends Fragment {

	ArrayList<String> N = new ArrayList<String>();
	ArrayList<String> Z = new ArrayList<String>();
	ArrayList<String> Bl = new ArrayList<String>();
	ArrayList<String> Br = new ArrayList<String>();
	ArrayAdapter<String> myArrAd;
	TextView TV1;
	String s="";
    Boolean b=false;
	
    //private MMAdView adViewFromXml;
	//MMRefreshHandler handler;
	
	public CalcFragment() {
        // Empty constructor required for fragment subclasses
    }
		
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	getActivity().setTitle(R.string.calculator);
        View rootView = inflater.inflate(R.layout.calc_main2, container, false);
        TV1=(TextView) rootView.findViewById(R.id.CalcTV1);
        
        rootView.findViewById(R.id.calc_btn_0).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_0).setId(0);
        rootView.findViewById(R.id.calc_btn_1).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_1).setId(1);
        rootView.findViewById(R.id.calc_btn_2).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_2).setId(2);
        rootView.findViewById(R.id.calc_btn_3).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_3).setId(3);
        rootView.findViewById(R.id.calc_btn_4).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_4).setId(4);
        rootView.findViewById(R.id.calc_btn_5).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_5).setId(5);
        rootView.findViewById(R.id.calc_btn_6).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_6).setId(6);
        rootView.findViewById(R.id.calc_btn_7).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_7).setId(7);
        rootView.findViewById(R.id.calc_btn_8).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_8).setId(8);
        rootView.findViewById(R.id.calc_btn_9).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_9).setId(9);
        rootView.findViewById(R.id.calc_btn_c).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_c).setId(10);
        rootView.findViewById(R.id.calc_btn_div).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_div).setId(11);
        rootView.findViewById(R.id.calc_btn_equally).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_equally).setId(12);
        rootView.findViewById(R.id.calc_btn_min).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_min).setId(13);
        rootView.findViewById(R.id.calc_btn_mul).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_mul).setId(14);
        rootView.findViewById(R.id.calc_btn_plus).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_plus).setId(15);
        rootView.findViewById(R.id.calc_btn_point).setOnClickListener(CalcOclBtn);
        rootView.findViewById(R.id.calc_btn_point).setId(16);
		  
		/*adViewFromXml = (MMAdView) rootView.findViewById(R.id.adViewCalc);
		Map<String, String> metaData = createMetaData();

		MMRequest request = new MMRequest();
		request.setMetaValues(metaData);
		adViewFromXml.setMMRequest(request);

		// (Optional) Set an event listener.
		// See AdListener.java for a basic implementation
		adViewFromXml.setListener(new MMAdListener());

		adViewFromXml.getAd();

		// (Optional) Setup an automatic refresh timer
		handler = new MMRefreshHandler(adViewFromXml);*/
        return rootView;
    }
    
    public OnClickListener CalcOclBtn = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch(v.getId()){
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				//|1|2|3|4|5|6|7|8|9
				//&1&2&3&4&5&6&7&8&9:
					b=false;
					s=s+String.valueOf(v.getId());
					TV1.setText(TV1.getText()+String.valueOf(v.getId()));
				break;
				case 10:
					N.clear();
					Z.clear();
					s="";
					TV1.setText("");
				break;
				case 11:
					if (N.size()==0&&s.length()==0||N.size()==0&&s.length()==1&&s.equals("-")){} else{
						
						if (b==false && s.length()!=0){
							N.add(s);
							s=""; }
						b=true;
						Z.add("/");

						TV1.setText(TV1.getText()+"/");
						
						}
				break;
				case 12:
					if (Z.size()!=0)
						if (N.size()==0&&s.length()==0||N.size()==0&&s.length()==1&&s.equals("-")){} else{
					
						if (b==false && s.length()!=0){
							N.add(s);
							s=""; }
		
						do{
							b=false;
							ym(0,N.size()-1);
						}
						while(b);
						
						do{
							b=false;
							pl(0,N.size()-1);
						}
						while(b);
					
							if (N.get(0).substring(N.get(0).length()-2,N.get(0).length()).equals(".0"))
								N.set(0,N.get(0).substring(0,N.get(0).length()-2));
						TV1.setText(N.get(0));
						s=N.get(0);
						
						N.clear();
						Z.clear();
					
						
						}
				break;
				case 13:
					if (N.size()==0&&s.length()==0){
						b=false;
						s=s+"-";
						//N.add("2");
						TV1.setText(TV1.getText()+"-");}
						
						
						if (N.size()==0&&s.length()==0||N.size()==0&&s.length()==1&&s.equals("-")){
						/*	b=false;
							s=s+"-";
							//N.add("2");
							TV1.setText(TV1.getText()+"-");*/
							
							
						} else{
							
						if (b==false && s.length()!=0){
							N.add(s);
							s=""; }
						b=true;
						Z.add("-");

						TV1.setText(TV1.getText()+"-");
						
						}
				break;
				case 14:
					if (N.size()==0&&s.length()==0||N.size()==0&&s.length()==1&&s.equals("-")){} else{
						
						if (b==false && s.length()!=0){
							N.add(s);
							s=""; }
						b=true;
						Z.add("*");

						TV1.setText(TV1.getText()+"*");
						}
				break;
				case 15:
					if (N.size()==0&&s.length()==0||N.size()==0&&s.length()==1&&s.equals("-")){} else{
						
						
						if (b==false && s.length()!=0){
							N.add(s);
							s=""; }
							b=true;
						Z.add("+");
						
						TV1.setText(TV1.getText()+"+");
						
						}
				break;
				case 16:
					Boolean d=false;
					for (int i=0;i<10;i++)
						if (s.indexOf(String.valueOf(i))>-1) d=true;
					
					if (s.indexOf(".")==-1&&d){		
					b=false;
					s=s+".";
					//N.add("2");
					TV1.setText(TV1.getText()+".");}
				break;
				
				
			}
		}
	};
    
	public void pl(int n, int k){
		for (int i=n;i<k;i++){
		//for (int i=0;i<N.size()-1;i++){
			if (Z.get(i).equals("+"))//||Z.get(i)=="-")
			{		
			    N.set(i,String.valueOf (Double.parseDouble(N.get(i))+Double.parseDouble(N.get(i+1))));
				N.remove(i+1);
				Z.remove(i);
				b=true;
				break;
				}
			if (Z.get(i).equals("-"))//||Z.get(i)=="-")
			{		
			    N.set(i,String.valueOf (Double.parseDouble(N.get(i))-Double.parseDouble(N.get(i+1))));
				N.remove(i+1);
				Z.remove(i);
				b=true;
				break;
			}
				}
	}
	
	public void ym(int n, int k){
		for (int i=n;i<k;i++){
			if (Z.get(i).equals("*"))//||Z.get(i)=="-")
			{		
			    N.set(i,String.valueOf (Double.parseDouble(N.get(i))*Double.parseDouble(N.get(i+1))));
				N.remove(i+1);
				Z.remove(i);
				b=true;
				break;
			}
			if (Z.get(i).equals("/"))//||Z.get(i)=="-")
			{		
			    N.set(i,String.valueOf (Double.parseDouble(N.get(i))/Double.parseDouble(N.get(i+1))));
				N.remove(i+1);
				Z.remove(i);
				b=true;
				break;
			}
		}
	}
	
	public void br(){
		//int min=-1;
		//int max=-1;
		//for (int i=Bl.size()-1;i>-1;i--){
		//for (int i=0;i<Bl.size()-1;i++){
			if (Bl.size()>0){
			//Bl.get(Bl.size()-1);
			
			if (Br.get(Br.size()-1).equals("-1")) Br.set(Bl.size()-1,String.valueOf(N.size()-1));
			
				do{
					b=false;
					ym(Integer.parseInt( Bl.get(Bl.size()-1)),Integer.parseInt(Br.get(Bl.size()-1)));
				}
				while(b);

				do{
					b=false;
				//	pl(0,N.size()-1);
					pl(Integer.parseInt( Bl.get(Bl.size()-1)),Integer.parseInt(Br.get(Bl.size()-1)));	
				}
				while(b);
				
			Bl.remove(Bl.size()-1);
			Br.remove(Br.size()-1);
			b=true;
			
			}
			
		/*	if (B.get(i).equals("("))//||Z.get(i)=="-")
			{		
			
			
			 //   N.set(i,String.valueOf (Double.parseDouble(N.get(i))*Double.parseDouble(N.get(i+1))));
				//N.remove(i+1);
				//Z.remove(i);
				//b=true;
				//break;
				min=i;
			}
			
			if (B.get(i).equals(")"))//||Z.get(i)=="-")
			{		
			max=i;
			  /*  N.set(i,String.valueOf (Double.parseDouble(N.get(i))/Double.parseDouble(N.get(i+1))));
				N.remove(i+1);
				Z.remove(i);
				b=true;
				break;*
			}
		}
		s (min!=-1){
			if (max<min) max=N.size()-1;
		} else break;
		*/
	}
	
    /*@Override
	public void onResume()
	{
		super.onResume();
		if(handler != null)
			handler.onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if(handler != null)
			handler.onPause();
	}
	
	protected Map<String, String> createMetaData()
	{
		Map<String, String> metaData = new HashMap<String, String>();
		metaData.put(MMRequest.KEY_AGE, "45");
		metaData.put(MMRequest.KEY_GENDER, MMRequest.GENDER_MALE);
		metaData.put(MMRequest.KEY_ZIP_CODE, "21224");
		metaData.put(MMRequest.KEY_MARITAL_STATUS, MMRequest.MARITAL_SINGLE);
		metaData.put(MMRequest.KEY_ORIENTATION, MMRequest.ORIENTATION_STRAIGHT);
		metaData.put(MMRequest.KEY_ETHNICITY, MMRequest.ETHNICITY_HISPANIC);
		metaData.put(MMRequest.KEY_INCOME, "50000");
		metaData.put(MMRequest.KEY_CHILDREN, "yes");
		metaData.put(MMRequest.KEY_POLITICS, "other");
		metaData.put(MMRequest.KEY_KEYWORDS, "soccer");
		return metaData;
	}*/
}
