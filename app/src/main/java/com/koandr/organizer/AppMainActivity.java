package com.koandr.organizer;

import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.*;
import android.support.v7.app.ActionBarActivity;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.view.*;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class AppMainActivity extends ActionBarActivity {

	public static String MY_PREF = "MY_PREF";
	int firstFragment = -1;
	boolean isFirstFragment = true, isFirstScreen;
	Fragment fragment;
	AppSidebarMenu parentContent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.app_main4);
		if (findViewById(R.id.listView) == null) {
			ViewGroup content = (ViewGroup) findViewById(android.R.id.content).getParent();
			ViewGroup basis = (ViewGroup) content.getParent();
			parentContent = new AppSidebarMenu(this);
			parentContent.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			basis.addView(parentContent, 0);
			basis.removeView(content);
			View slidingMenu = getLayoutInflater().inflate(R.layout.app_sidebar_menu, parentContent, false);
			FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			slidingMenu.setLayoutParams(params);
			content.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			parentContent.addView(slidingMenu, 0);
			parentContent.addView(content, 1);
		}
		
		String [] titles = new String[] { getString(R.string.file_manager), getString(R.string.news_rss), 
				getString(R.string.music), getString(R.string.notes), getString(R.string.calendar),
				getString(R.string.text_editor), getString(R.string.stopwatch), 
				getString(R.string.ftp_server), getString(R.string.preferences) };
	    int [] images = new int[] { R.drawable.slidingmenu_item_0, R.drawable.slidingmenu_item_1, 
				R.drawable.slidingmenu_item_2, R.drawable.slidingmenu_item_3, R.drawable.slidingmenu_item_4,
				R.drawable.slidingmenu_item_5, R.drawable.slidingmenu_item_6, R.drawable.slidingmenu_item_7, 
				R.drawable.slidingmenu_item_8 };
		
		ListView menu = (ListView) findViewById(R.id.listView);
		menu.setAdapter(new AppSidebarMenuAdapter(this, titles, images));
		menu.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	if (isFirstScreen) {
    				selectItem(position);
    				if (parentContent != null)
        				parentContent.toggle();
    			} else {
    				Intent intent = new Intent();
    				intent.putExtra("screen", position);
    				setResult(-1, intent);
    				finish();
    			}
            }
            
        });
     	
		if (getIntent().getIntExtra("screen", -1) == -1) {
			isFirstScreen = true;
	     	loadPreferences();
            if (savedInstanceState == null) {
	            selectItem(firstFragment);
	        } else {
                fragment = getSupportFragmentManager().getFragment(savedInstanceState, "fragment");
            }
		} else {
			isFirstScreen = false;
            if (savedInstanceState == null) {
			    selectItem(getIntent().getIntExtra("screen", -1));
            } else {
                fragment = getSupportFragmentManager().getFragment(savedInstanceState, "fragment");
            }
		}
     	if (isFirstScreen & parentContent != null | !isFirstScreen) {
	        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	     	getSupportActionBar().setHomeButtonEnabled(true);
     	}
     	if (isFirstScreen & parentContent != null) {
     		getSupportActionBar().setIcon(R.drawable.ic_menu);
     	} else {
            getSupportActionBar().setIcon(R.drawable.ic_about);
        }
     	
     	findViewById(R.id.ads_group).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Uri uriUrl = Uri.parse("https://vk.com/public67996546"); 
				Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl); 
				startActivity(launchBrowser);
			}
			
		});
     	 
		 AdView adView = (AdView) findViewById(R.id.adView);
	     AdRequest adRequest = new AdRequest.Builder().build();
	     adView.loadAd(adRequest);
	}

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "fragment", fragment);
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case android.R.id.home:
	    		if (!isFirstScreen) {
	    			finish();
	         	} else if (parentContent != null) {
	    			parentContent.toggle();
	         	}	    
	    		break;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	private void selectItem(int position) {
    	if (firstFragment != position | firstFragment == position & isFirstFragment) {
            firstFragment = position;
            if (isFirstFragment) {
    			isFirstFragment = false;
    		} else {
                if (position != 8) {
    				savePreferences();
    			}
    		}
    		setVolumeControlStream(AudioManager.STREAM_NOTIFICATION);
	    	android.support.v7.app.ActionBar ab = getSupportActionBar();
	        ab.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_STANDARD);
			FragmentManager fragmentManager = getSupportFragmentManager();
			for (int i = 1; i < fragmentManager.getBackStackEntryCount(); i++) {
				fragmentManager.popBackStack();
			}
			//fragmentManager.addOnBackStackChangedListener(null);
	        fragmentManager.addOnBackStackChangedListener(new FragmentManager. OnBackStackChangedListener() {
				@SuppressWarnings("deprecation")
				@Override
				public void onBackStackChanged() {
					if(getSupportFragmentManager().getBackStackEntryCount() == 0) {
						int mode = Activity.MODE_PRIVATE;
						SharedPreferences settings = getSharedPreferences(MY_PREF, mode);
						boolean c_d = settings.getBoolean("com_dialogV10", false);
						if (!c_d) {
							showDialog(0); 
						} else { 
							finish();
						}
					}
				}
			});
	        
	        
	    	//Fragment
	    	fragment = null;
	    	switch (position) {
                //Sliding menu
                case AppConst.FileManager:
                    fragment = new FileManagerFragment();
                    break;
	        	case AppConst.NewsRSS:
	        		fragment = new NewsRSSFragment();
					break;
	    		case AppConst.Music:
	    			fragment = new MusicPlayerMainFragment();
					break;
	    		/*case 3:
	    			//fragment=new FTPServerFragment();
	    			//fragment=new PaintFragment();
	        		startActivity(new Intent(this, PaintStartActivity.class));
					break;*/
	    		case AppConst.Notes:
	    			fragment = new NotesFragment();
	    			//Intent i = new Intent(this, NotesStartActivity.class);
	        		//startActivity(new Intent(this, NotesStartActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
					break;
	    		case AppConst.Calendar:
	        		fragment = new CalendarFragment();
					break;
	    		case AppConst.TextEditor:
	        		fragment = new TextEditorFragment();
					break;
	        	case AppConst.Stopwatch:
	        		fragment = new StopwatchFragment();
					break;
                case AppConst.FTPServer:
                    fragment = new FTPServerFragment();
                    break;
	        	case AppConst.Preferences:
	        		fragment = new PreferencesFragment();
					break;
                //Secondary screens
	        	case AppConst.NewsRSSBookmarks:
	        		fragment = new NewsRSSBookmarksFragment();
					break;
	        	case AppConst.NewsRSSBrowser:
	        		fragment = new NewsBrowserFragment();
					break;
                case AppConst.NotesViewer:
                    fragment = new NotesViewerFragment();
                    break;
	        	case AppConst.NotesEditor:
	        		fragment = new NotesEditorFragment();
					break;
	    	}
	        if (fragment!=null)
				fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    	}
    }
	
	public void loadPreferences() {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getSharedPreferences(MY_PREF, mode); 
		firstFragment = mySharedPreferences.getInt("firstFragmentV12", 0);
	}

	public void savePreferences() {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getSharedPreferences(MY_PREF, mode); 	
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("firstFragmentV12", firstFragment);
		editor.commit();
	}	
	
	@SuppressWarnings("deprecation")
	public void onBackPressed() {
        if (firstFragment == AppConst.FileManager) {
            if (((FileManagerFragment) fragment).onBackPressed()) {
                return;
            }
        }
		if (isFirstScreen) {
            int mode = Activity.MODE_PRIVATE;
            SharedPreferences settings = getSharedPreferences(MY_PREF, mode);
            Boolean c_d = false;
            c_d = settings.getBoolean("com_dialogV10", false);
            if (!c_d) {
                showDialog(0);
            } else {
                finish();
            }
		} else {
			setResult(0);
			finish();
		}
	}

	public void onSelectItem(int resultCode, Intent data) {
		if (resultCode == -1) {
			if (isFirstScreen) {
				selectItem(data.getIntExtra("screen", 0));
			} else {
				Intent intent = new Intent();
				intent.putExtra("screen", data.getIntExtra("screen", 0));
				setResult(-1, intent);
				finish();
			}
		}
	}
	
	@Override   
	public Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder = null;
		switch(id){
			case 0:
				builder=new AlertDialog.Builder(this);
			    builder.setTitle(R.string.rate_dialog);
				builder.setPositiveButton(R.string.rate_dialog_yes, new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int whichButton) {
						Intent intent=new Intent(Intent.ACTION_VIEW);
						intent.setData(Uri.parse("market://details?id=com.koandr.organizer")); 
						startActivity(intent);	
						int mode = Activity.MODE_PRIVATE;
						SharedPreferences mySharedPreferences = getSharedPreferences(MY_PREF, mode); 	
						SharedPreferences.Editor editor = mySharedPreferences.edit();
						editor.putBoolean("com_dialogV10", true);
	            		editor.commit();
	            		System.exit(0);
					}
					
				});
				builder.setNegativeButton(R.string.rate_dialog_neutral, new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int whichButton) {
						finish();
					}
					
				});
				builder.setNeutralButton(R.string.rate_dialog_no, new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int whichButton) {
						int mode = Activity.MODE_PRIVATE;
						SharedPreferences mySharedPreferences = getSharedPreferences(MY_PREF, mode); 	
						SharedPreferences.Editor editor = mySharedPreferences.edit();
						editor.putBoolean("com_dialogV10", true);
	            		editor.commit();
				        System.exit(0);
					}
					
				});
				break;
		}
		return builder.create();
	}	
	
}
