package com.koandr.organizer;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;


public class PaintFragment extends Fragment 
//implements OnGestureListener 
{

		//private GestureDetector gestureScanner;
	
	    public Boolean znc=false;
	    public Boolean ins=false;
	    public int touchX,touchY=0;
	    public float yr;
	    public int i,q,n,h=0;
	    public int colorT=Color.BLACK;
	    public int colorF=Color.WHITE;
	    public int s=20;
	    public int s2=1;
		public int openf=300;
		public int max=0;
	    public SeekBar seekBar,sB;
	    public View view;
	    public Bitmap bitmap;
	    public String path="";
	    public boolean runFlag=false;	
	    public Bitmap picture;	
		AlertDialog.Builder builder;
		public ArrayList<Float> x = new ArrayList<Float>();	
		public ArrayList<Float> y = new ArrayList<Float>();
	    public ArrayList<Float> size = new ArrayList<Float>();	
		public ArrayList<Integer> colorL = new ArrayList<Integer>();
		public ArrayList<Integer> Bbuf = new ArrayList<Integer>();
		public ArrayList<Integer> endf=new ArrayList<Integer>();
		public MySurfaceView sv;
		
		public int color;
		//public View colorView,cV;
		//public ImageView colormap;
	
	public PaintFragment() {
        // Empty constructor required for fragment subclasses
		//gestureScanner = new GestureDetector(this);
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	getActivity().setTitle(R.string.paint);
        //View rootView = inflater.inflate(R.layout.paint_main, container, false);
        
        Display display = getActivity().getWindowManager().getDefaultDisplay();
	//gestureScanner = new GestureDetector(this);
	
	loadPreferences();
	//LayoutInflater factory = LayoutInflater.from(getActivity());
	//colorView = factory.inflate(R.layout.paint_choosecolor, null);
	//cV= factory.inflate(R.layout.paint_settings, null);
	//colormap = (ImageView) colorView.findViewById(R.id.colormap);
 	if (display.getWidth()>display.getHeight())	
		max=display.getWidth(); 
		else max=display.getHeight();		
		picture = Bitmap.createBitmap(display.getWidth(), display.getHeight(),//max, max,
				Bitmap.Config.ARGB_8888);	
	//else picture = Bitmap.createBitmap(display.getHeight(), display.getHeight(),Bitmap.Config.ARGB_8888);
	sv=	new MySurfaceView(getActivity());
	LinearLayout.LayoutParams lParamsL = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
	sv.setLayoutParams(lParamsL);
	//setContentView(sv);//new MySurfaceView(this));
	//sv = (MySurfaceView) rootView.findViewById(R.id.paint_main_surfaceview);
	 sv.
	 setOnTouchListener(new View.OnTouchListener() {
         @Override
         public boolean onTouch(View v, MotionEvent event) {
        	 
        	 switch (event.getAction()){		
      	   case MotionEvent.ACTION_DOWN:
      				x.add(event.getX());
      				y.add(event.getY());
      				size.add((float)s);
      				colorL.add(colorT);
      				i++;	   
      	   Bbuf.add(i);	   
      	   q++;
      	   break;
      		case MotionEvent.ACTION_MOVE:	
      		{
      			x.add(event.getX());
      			y.add(event.getY());
      			size.add((float)s);
      			colorL.add(colorT);
      			i++;
      			sv.
      			drawThread.draws();
      			break;
      			}
      			case MotionEvent.ACTION_UP:	
      			endf.add(i);	
      			break;
      		}
        	 
        	 
             return true;
            		 //gestureScanner.onTouchEvent(event);
         }
     }); 

	setHasOptionsMenu(true);
    return sv;//rootView;
}
	
	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    // TODO Add your menu entries here
		
	    super.onCreateOptionsMenu(menu, inflater);
	    //getActivity().getMenuInflater()
	    inflater.inflate(R.menu.paint_menu, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    //Log.d("MENU", "Cliced MenuItem is " + item.getTitle());
		if (item.getItemId()==R.id.action_item_1){
			DialogFragment dialogFrag = NotebookDialogAdd.newInstance(123);
            dialogFrag.setTargetFragment(PaintFragment.this, 1);
            
            //(this, DIALOG_FRAGMENT);
            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");
		}
	    return super.onOptionsItemSelected(item);
	}*/
	
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.paint_menu, menu);
	    super.onCreateOptionsMenu(menu,inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    //Log.d("MENU", "Cliced MenuItem is " + item.getTitle());
		if (item.getItemId()==R.id.action_item_1){
			if (q>0) {	
		        int	b=Bbuf.get(q-1);
		    	Bbuf.remove(q-1);	
		    	endf.remove(q-1);	
		    	q--;
		    	for (int j = i-1; j>b-2; j--){
	    			x.remove(j);
	    			y.remove(j);
	    			size.remove(j);
	    			colorL.remove(j);
	    		}
	    		i=b-1;
	    		n=3;
	    		//Toast.makeText(getApplicationContext(), R.string.cancel, Toast.LENGTH_SHORT).show();			
	    		sv.drawThread.draws();
	    	}
		}
		if (item.getItemId()==R.id.action_item_2){
			DialogFragment dialogFrag = PaintDialogPreferences.newInstance(123);
            dialogFrag.setTargetFragment(PaintFragment.this, 1);
            
            //(this, DIALOG_FRAGMENT);
            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");
		}
	    return super.onOptionsItemSelected(item);
	}
	
	//int burch, bground, 
	int burch, seekbar;//, cancel;
	
	public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (resultCode == Activity.RESULT_OK) {
			if(requestCode == openf){ 
			/*	
					for (int j = i-1; j>-1; j--){
						x.remove(j);
						y.remove(j);
						size.remove(j);
						colorL.remove(j);
					}
					i=0;
			*/
				Uri ChossefileUri = data.getData(); 
				if(ChossefileUri !=null){ 
					Uri fileUri = ChossefileUri;
					if(fileUri != null) 
					{
						Toast.makeText(getActivity(), getRealPathFromURI(fileUri), Toast.LENGTH_SHORT).show();
	   
					path=getRealPathFromURI(fileUri);
				//	showDialog(5);
				//	setContentView(sv);
				n=5;
					sv.drawThread.draws();
						}}}}
		
		
		
		if (requestCode == 3){

			int bground = data.getIntExtra("color", 0);
	    	if (bground!=0) colorF=bground;
			

			DialogFragment dialogFrag = PaintDialogPreferences.newInstance(123);
            dialogFrag.setTargetFragment(PaintFragment.this, 1);
            
            //(this, DIALOG_FRAGMENT);
            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");
		}
		
		if (requestCode == 2){

			//int
			burch = data.getIntExtra("color", 0);
			if (burch!=0) {
				colorT=burch;
	    		savePreferences();
	    	}

			DialogFragment dialogFrag = PaintDialogPreferences.newInstance(123);
            dialogFrag.setTargetFragment(PaintFragment.this, 1);
            
            //(this, DIALOG_FRAGMENT);
            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");
		}
		
	    if (requestCode == 1){
	    	
		    //data.getStringExtra("event");
	    	//burch = data.getIntExtra("burch", 0);
	    	//bground = data.getIntExtra("bground", 0);
	    	seekbar = data.getIntExtra("seekbar", 0);
	    	if (seekbar!=0){
	    		s=seekbar;
	    		savePreferences();
	    	}
	    	//cancel = data.getIntExtra("cancel", 0);
	    	
	    	/*if (burch!=0) colorT=burch;
	    	if (bground!=0) colorF=bground;
	    	if (seekbar!=0){
	    		s=seekbar;
	    		savePreferences();
	    	}*/
	    	
	    	/*for (int i=0;i<cancel;i++)
	    	if (q>0) {	
		        int	b=Bbuf.get(q-1);
		    	Bbuf.remove(q-1);	
		    	endf.remove(q-1);	
		    	q--;
		    	for (int j = i-1; j>b-2; j--){
	    			x.remove(j);
	    			y.remove(j);
	    			size.remove(j);
	    			colorL.remove(j);
	    		}
	    		i=b-1;
	    		n=3;
	    		//Toast.makeText(getApplicationContext(), R.string.cancel, Toast.LENGTH_SHORT).show();			
	    		sv.drawThread.draws();
	    	}*/
	    	if (data.getStringExtra("event").equals("burch")) {
		    	DialogFragment dialogFrag = PaintDialogColor.newInstance(123);
	            dialogFrag.setTargetFragment(PaintFragment.this, 2);
	            //(this, DIALOG_FRAGMENT);
	            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");
	    	}
	    	
	    	if (data.getStringExtra("event").equals("bground")) {
		    	DialogFragment dialogFrag = PaintDialogColor.newInstance(123);
	            dialogFrag.setTargetFragment(PaintFragment.this, 3);
	            //(this, DIALOG_FRAGMENT);
	            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");
	    	}
	    	
	    	if (data.getStringExtra("event").equals("open")) {
	    		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("image/*");
				startActivityForResult(intent, openf); 
	    	}
	    	
	    	if (data.getStringExtra("event").equals("save")) {
	    		toFile();
	    	}
	    	
	    	if (data.getStringExtra("event").equals("clear")) {
	    		n=4;	
	    		sv.drawThread.draws();
			}
	    	
	    } //make sure fragment codes match up {
	        //String editText = data.getStringExtra(KEY);
	}
	/*public boolean onTouchEvent(MotionEvent event)
	{
	switch (event.getAction()){		
	   case MotionEvent.ACTION_DOWN:
				x.add(event.getX());
				y.add(event.getY());
				size.add((float)s);
				colorL.add(colorT);
				i++;	   
	   Bbuf.add(i);	   
	   q++;
	   break;
		case MotionEvent.ACTION_MOVE:	
		{
			x.add(event.getX());
			y.add(event.getY());
			size.add((float)s);
			colorL.add(colorT);
			i++;
			sv.
			drawThread.draws();
			break;
			}
			case MotionEvent.ACTION_UP:	
			endf.add(i);	
			break;
		}
		return gestureScanner.onTouchEvent(event);// true;
	}*

	@Override
	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}*/
	
	private void toFile() {
		Canvas canvas = null; 
		Bitmap bmpBase = null;
		Display	display = getActivity().getWindowManager().getDefaultDisplay();
		bmpBase = Bitmap.createBitmap(display.getWidth(), display.getHeight(), Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bmpBase);	
		canvas.drawColor(colorF);
		canvas.drawBitmap(picture, 0,0,	null);
		String newFolder = "/Apps++";//"/Organizer Paint";
			try{
				String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
				File myNewFolder = new File(extStorageDirectory + newFolder);
				myNewFolder.mkdir();
				bmpBase.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(new File("sdcard/Apps++/" //Organizer Paint/
						+"Picture_"+String.valueOf(s2)+".jpg")));
			//	if (picture==null)
				Toast.makeText(getActivity().getApplicationContext(), "File saved: /sdcard/Apps++/Picture_"+String.valueOf(s2)+".jpg", Toast.LENGTH_SHORT).show();		
				s2++;
				savePreferences();
          } catch (Exception e) {
        	  
          }
	}
	
	public static String MY_PREF = "MY_PREF";
	public void loadPreferences() 
	{
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getActivity().getSharedPreferences(MY_PREF, mode); 
		burch = mySharedPreferences.getInt("PaintBurch", Color.BLACK);
		colorT=burch;
		s= mySharedPreferences.getInt("PaintSizeBurch", 15);
		s2= mySharedPreferences.getInt("PaintCountFile",1);
	}

	protected void savePreferences() 
	{
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getActivity().getSharedPreferences(MY_PREF, mode); 	
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("PaintBurch", burch);
		editor.putInt("PaintSizeBurch", s);
		editor.putInt("PaintCountFile",s2);
		editor.commit();
	}	

	public class MySurfaceView extends SurfaceView implements SurfaceHolder.Callback {
		private DrawThread drawThread;

		public MySurfaceView(Context context) {
			super(context);
			getHolder().addCallback(this);
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

		@Override
		public void surfaceCreated(SurfaceHolder holder) {	
	     //	Display	display = getWindowManager().getDefaultDisplay();
		//	picture = Bitmap.createBitmap(display.getWidth(), display.getHeight(),Bitmap.Config.ARGB_8888);	
			
			if (drawThread!=null) drawThread=null;			
			drawThread = new DrawThread(getHolder());
			runFlag=true;
			drawThread.start();	
			if (n==5) drawThread.draws();
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
		    boolean retry = true;
			runFlag=false;
			while (retry) {
				try {
					drawThread.join();
					retry = false;
				} catch (InterruptedException e) {}
			}
		}
				
		///////
	//	public
		class DrawThread extends Thread{
			private SurfaceHolder surfaceHolder;
			private Canvas canvass;

			public DrawThread(SurfaceHolder surfaceHolder){	this.surfaceHolder = surfaceHolder;	}
			public void draws(){
				if (runFlag){
					
					if (n==4||n==5){		
						for (int j = i-1; j>-1; j--){
							x.remove(j);
							y.remove(j);
							size.remove(j);
							colorL.remove(j);
						}			 
					//	path="";
						i=0;	 
						for (int j=q-1; j>-1; j--){
							endf.remove(j);
							Bbuf.remove(j);
						}		 
						q=0;
						colorF=Color.WHITE;								
					}
					if (n==4) path="";
					
					
				//	Paint p=new Paint();		
				//	canvass = new Canvas(picture);
					if (n==3||n==4||n==5)
						picture = Bitmap.createBitmap(max, max,Bitmap.Config.ARGB_8888);
						canvass = new Canvas(picture);
				     	Paint p=new Paint();	
						
				/*	 if (n==1){
						 Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
						 intent.setType("image/*");
						 startActivityForResult(intent, openf); 		
						 n=5;	 
					 }*/
					 
					 
			//		 if (n==4) colorF=Color.WHITE;			 
					
					if (n==3||n==5){
						if (path!=""){
					//		picture = Bitmap.createBitmap(max, max,Bitmap.Config.ARGB_8888);
				//			canvass = new Canvas(picture);
				         	Display	display = getActivity().getWindowManager().getDefaultDisplay();
							if 
							( (BitmapFactory.decodeFile(path)).getWidth()>display.getWidth() ||(BitmapFactory.decodeFile(path)).getHeight()>display.getHeight()){
								Rect dest = new Rect(0, 0, display.getWidth(), display.getHeight());
								p.setFilterBitmap(true);
								//	canvas.drawBitmap(background, null, dest, paint);	
								canvass.drawBitmap(BitmapFactory.decodeFile(path),null,dest,p);

							} else
								canvass.drawBitmap(BitmapFactory.decodeFile(path),0,0,null);
						}

						/////////////////
					}
					if (n==3){

						for (int j = 0; j < i-1; j++){
							p.setAntiAlias(true);

							boolean pr=false;
							for (int j2=0; j2<q; j2++){
								if	(endf.get(j2)==j+1) {
									pr=true;
								}
							}
							if (pr==false){
								p.setColor(colorL.get(j));
								p.setStrokeWidth(size.get(j));
								canvass.drawLine(x.get(j),y.get(j),x.get(j+1),y.get(j+1),p);
								canvass.drawCircle(x.get(j),y.get(j),size.get(j)/2,p);
							} //else pr=false;

						}
					}







					if (n==0){
						p.setColor(colorL.get(i-2));
						p.setStrokeWidth(size.get(i-2));
						canvass.drawLine(x.get(i-1),y.get(i-1),x.get(i-2),y.get(i-2),p);
						canvass.drawCircle(x.get(i-2),y.get(i-2),size.get(i-2)/2,p);
					}
					n=0;
				}
			}

			@Override
			public void run() {
				Canvas canvas=null;
				while (runFlag) {
					try {
						canvas = surfaceHolder.lockCanvas(null);
						synchronized (surfaceHolder) {
							canvas.drawColor(colorF);
							canvas.drawBitmap(picture, 0,0,	null);
						}
					} catch (Exception e) {} 					
					finally {
						if (canvas != null) {
							surfaceHolder.unlockCanvasAndPost(canvas);
						}}}
			}				
		}//?
	}
	
}
