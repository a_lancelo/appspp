package com.koandr.organizer;

import java.io.*;
import java.net.*;

public class FTPServer extends Thread {

	FTPServerTcpListener wifiListener;
	boolean shouldExit = false;

	@Override
	public void run() {			
		while (!shouldExit) {
			if (wifiListener != null) {
				if (!wifiListener.isAlive()) {
					try {
						wifiListener.join();
					} catch (InterruptedException e) {

					}
					wifiListener = null;
				}
			}
			if (wifiListener == null) {
				ServerSocket listenSocket = null;
				try{
					listenSocket = new ServerSocket(2020);
				} catch(IOException e) {

				}
				wifiListener = new FTPServerTcpListener(listenSocket);
				wifiListener.start();
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

			}
		}
		wifiListener.quit();		
	}

	@Override
	public void interrupt() {
		super.interrupt();			
		shouldExit = true;
	}

}
