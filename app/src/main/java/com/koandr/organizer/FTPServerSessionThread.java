package com.koandr.organizer;

import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

public class FTPServerSessionThread extends Thread {

	Socket socket, dataSocket;
	OutputStream dataOutputStream;		
	ServerSocket dataServer;		
	DataInputStream din;
	DataOutputStream dout;		
	final int USER = 0, PASS = 1, FEAT = 2, PWD = 3, 
	NOOP = 4, CWD = 5, SYST = 6, PASV = 7, LIST = 8, 
	MKD = 9, RMD = 10, TYPE = 11, STOR = 12, RETR = 13, RNFR = 14, 
	RNTO = 15, DELE = 16, CDUP = 17, SIZE = 18, OPTS = 19, SITE = 20;
	private String[] commands = {"USER", "PASS", "FEAT", "PWD",
		"NOOP", "CWD", "SYST", "PASV", "LIST", "MKD", "RMD",
		"TYPE", "STOR", "RETR", "RNFR", "RNTO", "DELE", "CDUP", "SIZE", "OPTS", "SITE"};
	String dir = "mnt/sdcard", root = "mnt/sdcard", renamePath;
	boolean binaryMode = true, shouldExit = false;

	public FTPServerSessionThread(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {			
		try {				
			writeString("220 A Done\r\n");				
			BufferedReader in = new BufferedReader(new InputStreamReader(
													   socket.getInputStream()), 8192);
			while (true) {
				String line;
				line = in.readLine();
				if (line != null) {
					makeCommand(line);						
				}
			}
		} catch (IOException e) {

		}
	}

	public void closeDataSocket() {
		if (dataOutputStream != null) {
			try {
				dataOutputStream.close();
			} catch (IOException e) {

			}
			dataOutputStream = null;
		}
		if (dataSocket != null) {
			try {
				dataSocket.close();
			} catch (IOException e) {

			}
		}
		dataSocket = null;
	}	

	public void closeSocket() {
		if (socket == null) {
			return;
		}
		try {
			socket.close();
		} catch (IOException e) {

		}
	}				

	private void makeCommand(String str) {
		String[] strings = str.split(" ");
		if (strings == null) {
			return;
		}
		if (strings.length < 1) {				
			return;
		}
		String verb = strings[0];
		if (verb.length() < 1) {
			return;
		}
		verb = verb.trim();
		verb = verb.toUpperCase();
		for (int i = 0; i < commands.length; i++) {
			if (commands[i].equals(verb)) {
				switch (i) {
					case USER:
						writeString("331 \r\n");
						break;
					case PASS:
						writeString("230 \r\n");
						break;
					case FEAT:
						writeString("211 \r\n");
						break;
					case PWD:											
						String currentDir = dir.substring(root.length());
						if (currentDir.length() == 0) {
							currentDir = "/";
						}
						writeString("257 \"" + currentDir + "\"\r\n");						
						break;
					case NOOP:
						writeString("200 \r\n");
						break;
					case CWD:
						dir = root + getPath(str);							
						writeString("250 \r\n");
						break;
					case SYST:
						writeString("215 UNIX Type: L8 \r\n");
						break;							
					case PASV:													
						int port = 0;
						try {
							dataServer = new ServerSocket(0, 5);
							port = dataServer.getLocalPort();								
						} catch (IOException e) {

						}	
						StringBuilder response = new StringBuilder("227 Entering Passive Mode (");
						response.append(getIpAddress().replace('.', ','));
						response.append(",");
						response.append(port / 256);
						response.append(",");
						response.append(port % 256);
						response.append(").\r\n");
						String responseString = response.toString();
						writeString(responseString);
						break;	
					case LIST:							
						writeString("150 Opening ASCII mode data connection for file list\r\n");
						File fileToList = new File(dir);
						if (fileToList.exists()) {
							String listing;							
							if (fileToList.isDirectory()) {
								StringBuilder response2 = new StringBuilder();
								listDirectory(response2, fileToList);
								listing = response2.toString();
							} else {
								listing = makeLsString(fileToList);
							}								
							sendDataSocket(listing);	
						}
						writeString("226 Data transmission OK\r\n");																					
						break;	
					case MKD:				
						new File(root + getPath(str)).mkdir();	
						writeString("250 Directory created\r\n");
						break;
					case RMD:
						new File(root + getPath(str)).delete();						
						writeString("250 Removed directory\r\n");
						break;
					case TYPE:
						String param = getUnitedText(str);
						if (param.equals("I") || param.equals("L 8")) {
							binaryMode = true;
							writeString("200 Binary type set\r\n");								
						} else if (param.equals("A") || param.equals("A N")) {
							binaryMode = false;
							writeString("200 ASCII type set\r\n");
						}
						break;
					case STOR:
						doStorOrAppe(getPath(str), false);
						break;
					case RETR:				
						FileInputStream in = null;
						try {
							in = new FileInputStream(new File(root + getPath(str)));
							byte[] buffer = new byte[65536];
							int bytesRead;
							startUsingDataSocket();
							writeString("150 Sending file\r\n");
							if (binaryMode) {
								while ((bytesRead = in.read(buffer)) != -1) {
									if (sendViaDataSocket(buffer, 0, bytesRead) == false); 									
								}
							} else {
								boolean lastBufEndedWithCR = false;
								while ((bytesRead = in.read(buffer)) != -1) {
									int startPos = 0, endPos = 0;
									byte[] crnBuf = { '\r', '\n' };
									for (endPos = 0; endPos < bytesRead; endPos++) {
										if (buffer[endPos] == '\n') {
											sendViaDataSocket(buffer, startPos, endPos
															  - startPos);
											if (endPos == 0) {
												if (!lastBufEndedWithCR) {
													sendViaDataSocket(crnBuf, 0, 1);
												}
											} else if (buffer[endPos - 1] != '\r') {
												sendViaDataSocket(crnBuf, 0, 1);
											}
											startPos = endPos;
										}
									}
									sendViaDataSocket(buffer, startPos, endPos
													  - startPos);
									if (buffer[bytesRead - 1] == '\r') {
										lastBufEndedWithCR = true;
									} else {
										lastBufEndedWithCR = false;
									}
								}
							}
						} catch (FileNotFoundException e) {

						} catch (IOException e) {

						} finally {
							try {
								if (in != null)
									in.close();
								dataOutputStream.close();
							} catch (IOException swallow) {
							}
						}
						writeString("226 Transmission finished\r\n");
						break;
					case RNFR:
						renamePath = root + getPath(str);
						writeString("350\r\n");
						break;
					case RNTO:
						if (renamePath != null) {
							File file = new File(renamePath);
							if (file.exists()) {																	
								file.renameTo(new File(root + getPath(str)));
							}
						}
						writeString("250 File rename successfully\r\n");
						break;
					case DELE:
						new File(root + getPath(str)).delete();						
						writeString("250 File deleted\r\n");
						break;
					case CDUP:
						dir = new File(dir).getParent();						
						writeString("200 CDUP successful\r\n");
						break;
					case SIZE:						
						writeString("213 "+new File(dir+getPath(str)).length()+"\r\n");
						break;
					case OPTS:
						writeString("200 \r\n");
						break;
					case SITE:
						writeString("200 \r\n");
						break;	
				}
			}
		}
	}

	private void sendDataSocket(String string) {			
		try {
			byte[] bytes = string.getBytes("UTF-8");
			dataSocket = dataServer.accept();
			dataOutputStream = dataSocket.getOutputStream();				
			dataOutputStream.write(bytes, 0, bytes.length);
			dataSocket.close();
		} catch (IOException e) {

		}			
	}	

	private void startUsingDataSocket() {
		try {
			dataSocket = dataServer.accept();
			dataOutputStream = dataSocket.getOutputStream();
		} catch (IOException e) {

		}		
	}

	public boolean sendViaDataSocket(byte[] bytes, int start, int len) {
		if (dataOutputStream == null) {
			return false;
		}
		if (len == 0) {
			return true; 
		}
		try {
			dataOutputStream.write(bytes, start, len);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public int receiveFromDataSocket(byte[] buf) {
		int bytesRead;
		if (dataSocket == null) {
			return -2;
		}
		if (!dataSocket.isConnected()) {
			return -2;
		}
		InputStream in;
		try {
			in = dataSocket.getInputStream();
			while ((bytesRead = in.read(buf, 0, buf.length)) == 0); 								
			if (bytesRead == -1) {
				return -1;
			}
		} catch (IOException e) {
			return 0;
		}
		return bytesRead;
	}		

	private void writeBytes(byte[] bytes) {
		try {
			BufferedOutputStream out = new BufferedOutputStream(
				socket.getOutputStream(), 65536);//8192)Defaults.dataChunkSize);
			out.write(bytes);
			out.flush();
		} catch (IOException e) {
			try {
				socket.close();
			} catch (IOException e2) {

			}		   
            return;
        }
	}

	private void writeString(String str) {
		byte[] strBytes;
		try {
			strBytes = str.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			strBytes = str.getBytes();
		}
		writeBytes(strBytes);
	}

	private String getIpAddress() {
		String ip = "";
		try {
			Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
				.getNetworkInterfaces();
			while (enumNetworkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = enumNetworkInterfaces
					.nextElement();
				Enumeration<InetAddress> enumInetAddress = networkInterface
					.getInetAddresses();
				while (enumInetAddress.hasMoreElements()) {
					InetAddress inetAddress = enumInetAddress.nextElement();
					if (inetAddress.isSiteLocalAddress()) {
						ip = inetAddress.getHostAddress();
					}
				}
			}
		} catch (SocketException e) {
			ip += "Something Wrong! " + e.toString() + "\n";
		}
		return ip;
	}

	public String listDirectory(StringBuilder response, File dir) {
        if (!dir.isDirectory()) {
            return "500 Internal error, listDirectory on non-directory\r\n";
        }
    	File[] entries = dir.listFiles();
        if (entries == null) {
            return "500 Couldn't list directory. Check config and mount status.\r\n";
        }
        try {
            Arrays.sort(entries, listingComparator);
        } catch (Exception e) {
			entries = dir.listFiles();
        }
        for (File entry : entries) {
            String curLine = makeLsString(entry);
            if (curLine != null) {
                response.append(curLine);
            }
        }
        return null;
    }

    protected String makeLsString(File file) {
        StringBuilder response = new StringBuilder();
        if (!file.exists()) {
            return null;
        }
	    String lastNamePart = file.getName();
        if (lastNamePart.contains("*") || lastNamePart.contains("/")) {
            return null;
        }
        if (file.isDirectory()) {
            response.append("drwxr-xr-x 1 owner group");
        } else {
            response.append("-rw-r--r-- 1 owner group");
        }

        // The next field is a 13-byte right-justified space-padded file size
        long fileSize = file.length();
        String sizeString = new Long(fileSize).toString();
        int padSpaces = 13 - sizeString.length();
        while (padSpaces-- > 0) {
            response.append(' ');
        }
        response.append(sizeString);

        // The format of the timestamp varies depending on whether the mtime
        // is 6 months old
        long mTime = file.lastModified();
        SimpleDateFormat format;
        // Temporarily commented out.. trying to fix Win7 display bug
        if (System.currentTimeMillis() - mTime > 1000*60*60*24*30*6) { // MS_IN_SIX_MONTHS) {
            // The mtime is less than 6 months ago
            format = new SimpleDateFormat(" MMM dd HH:mm ", Locale.US);
        } else {
            // The mtime is more than 6 months ago
            format = new SimpleDateFormat(" MMM dd  yyyy ", Locale.US);
        }
        response.append(format.format(new Date(file.lastModified())));
        response.append(lastNamePart);
        response.append("\r\n");
        return response.toString();
    }

	static final Comparator<File> listingComparator = new Comparator<File> (){
        @Override
        public int compare(File lhs, File rhs) {
            if (lhs.isDirectory() && rhs.isFile()) {
                return -1;
            } else if (lhs.isFile() && rhs.isDirectory()) {
                return 1;
            } else {
                return lhs.getName().compareToIgnoreCase(rhs.getName());
            }
        }
    };

	private String getUnitedText(String input) {
        if (input == null) {
            return "";
        }
        int firstSpacePosition = input.indexOf(' ');
        if (firstSpacePosition == -1) {
            return "";
        }
        String retString = input.substring(firstSpacePosition + 1);
		retString = retString.replaceAll("\\s+$", "");
	    return retString;
    }	

	private String getPath(String str) {
		String path = getUnitedText(str);
		if (path.charAt(0) != '/') {			
			path = dir.substring(root.length()) + "/" + path;
		}
		return path;
	}	
	
	public File inputPathToChrootedFile(File existingPrefix, String param) {
        try {
            if (param.charAt(0) == '/') {
                // The STOR contained an absolute path
                File chroot = new File(dir);//FsSettings.getChrootDir();
                return new File(chroot, param);
            }
        } catch (Exception e) {
        }

        // The STOR contained a relative path
        return new File(existingPrefix, param);
    }

	public void doStorOrAppe(String param, boolean append) {
      	File storeFile = new File(root + param);//	inputPathToChrootedFile(new File(dir), param);
        String errString = null;
        FileOutputStream out = null;
        storing: {
            if (storeFile.isDirectory()) {
                errString = "451 Can't overwrite a directory\r\n";
                break storing;
            }
            try {
                if (storeFile.exists()) {
                    if (!append) {
                        if (!storeFile.delete()) {
                            errString = "451 Couldn't truncate file\r\n";
                            break storing;
                        }
                    }
                }
                out = new FileOutputStream(storeFile, append);
            } catch (FileNotFoundException e) {
                try {
                    errString = "451 Couldn't open file \"" + param + "\" aka \""
						+ storeFile.getCanonicalPath() + "\" for writing\r\n";
                } catch (IOException io_e) {
                    errString = "451 Couldn't open file, nested exception\r\n";
                }
                break storing;
            }
         	startUsingDataSocket();
          	writeString("150 Data socket ready\r\n");
            byte[] buffer = new byte[65536];
            int numRead;
            while (true) {
              	switch (numRead = receiveFromDataSocket(buffer)) {
              		case -1:
						//Log.d(TAG, "Returned from final read");
						// We're finished reading
						break storing;
					case 0:
						errString = "426 Couldn't receive data\r\n";
						break storing;
					case -2:
						errString = "425 Could not connect data socket\r\n";
						break storing;
					default:
						try {
							if (binaryMode) {
								out.write(buffer, 0, numRead);
							} else {
								// ASCII mode, substitute \r\n to \n
								int startPos = 0, endPos;
								for (endPos = 0; endPos < numRead; endPos++) {
									if (buffer[endPos] == '\r') {
										// Our hacky method is to drop all \r
										out.write(buffer, startPos, endPos - startPos);
										startPos = endPos + 1;
									}
								}
								// Write last part of buffer as long as there was something
								// left after handling the last \r
								if (startPos < numRead) {
									out.write(buffer, startPos, endPos - startPos);
								}
							}                    
							out.flush();
						} catch (IOException e) {
							errString = "451 File IO problem. Device might be full.\r\n";
							StackTraceElement[] traceElems = e.getStackTrace();
							for (StackTraceElement elem : traceElems) {

							}
							break storing;
						}
						break;
                }
            }
        }
        try {
            if (out != null) {
                out.close();
            }
			dataOutputStream.close();
        } catch (IOException e) {

	    }

        if (errString != null) {
         	writeString(errString);
        } else {
          	writeString("226 Transmission complete\r\n");
        }
	}
	
}
