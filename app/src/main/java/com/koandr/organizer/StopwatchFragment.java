package com.koandr.organizer;

import java.util.ArrayList;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.*;


public class StopwatchFragment extends Fragment {
	//private static String TAG = "StopwatchActivity";
		
	// View elements in stopwatch.xml
	private TextView m_elapsedTime;
	private Button m_start;
	private Button m_pause;
	private Button m_reset;
	private Button m_lap;
	//private ArrayAdapter<String> m_lapList;
	private ArrayList<String> lapList = new ArrayList<String>();
	
	// Timer to update the elapsedTime display
    private final long mFrequency = 100;    // milliseconds
    private final int TICK_WHAT = 2; 
	private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
        	updateElapsedTime();
        	sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

    // Connection to the backgorund StopwatchService
	private StopwatchService m_stopwatchService;
	private ServiceConnection m_stopwatchServiceConn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			m_stopwatchService = ((StopwatchService.LocalBinder)service).getService();
			showCorrectButtons();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			m_stopwatchService = null;
		}
	};
	
	private void bindStopwatchService() {
        getActivity().bindService(new Intent(getActivity(), StopwatchService.class), 
        			m_stopwatchServiceConn, Context.BIND_AUTO_CREATE);
	}
	
	private void unbindStopwatchService() {
		if ( m_stopwatchService != null ) {
			getActivity().unbindService(m_stopwatchServiceConn);
		}
	}
	
    /*@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.stopwatch_main);
        
        */
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	getActivity().setTitle(R.string.stopwatch);
        View rootView = inflater.inflate(R.layout.stopwatch_main, container, false);
        
        final ListView lv = (ListView)rootView.findViewById(R.id.stopwatch_listview);
        
        lv.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.stopwatch_item, lapList));
        getActivity().startService(new Intent(getActivity(), StopwatchService.class));
        bindStopwatchService();
        
        m_elapsedTime = (TextView)rootView.findViewById(R.id.ElapsedTime);
        
        m_start = (Button)rootView.findViewById(R.id.start);
        m_pause = (Button)rootView.findViewById(R.id.pause);
        m_reset = (Button)rootView.findViewById(R.id.reset);
        m_lap = (Button)rootView.findViewById(R.id.lap);
        
		m_start.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					m_stopwatchService.start();
					showPauseLapButtons();
				}
			});
		m_pause.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					m_stopwatchService.pause();
					showStartResetButtons();
				}
			});
		m_reset.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					m_stopwatchService.reset();
					//m_lapList.clear();
					lapList.clear();

			        lv.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.stopwatch_item, lapList));
				}
			});
		m_lap.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					m_stopwatchService.lap();
					//m_
					lapList.add(String.valueOf(lapList.size()+1)+".  "+m_stopwatchService.getFormattedElapsedTime()); 	

			        lv.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.stopwatch_item, lapList));
				}
			});
		
        //m_lapList = (ArrayAdapter<String>)lv.getAdapter();

       mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
    
        return rootView;
    }
    
    @Override
	public void onDestroy() {
        super.onDestroy();
        unbindStopwatchService();
    }
    
    private void showCorrectButtons() {
    	//Log.d(TAG, "showCorrectButtons");
    	
    	if ( m_stopwatchService != null ) {
    		if ( m_stopwatchService.isStopwatchRunning() ) {
    			showPauseLapButtons();
    		} else {
    			showStartResetButtons();
    		}
    	}
    }
    
    private void showPauseLapButtons() {
    	//Log.d(TAG, "showPauseLapButtons");
    	
    	m_start.setVisibility(View.GONE);
    	m_reset.setVisibility(View.GONE);
    	m_pause.setVisibility(View.VISIBLE);
    	m_lap.setVisibility(View.VISIBLE);
    }
    
    private void showStartResetButtons() {
    	//Log.d(TAG, "showStartResetButtons");

    	m_start.setVisibility(View.VISIBLE);
    	m_reset.setVisibility(View.VISIBLE);
    	m_pause.setVisibility(View.GONE);
    	m_lap.setVisibility(View.GONE);
    }
    
  /*  public void onStartClicked(View v) {
    	//Log.d(TAG, "start button clicked");
    	m_stopwatchService.start();
    	
    	showPauseLapButtons();
    }
    
    public void onPauseClicked(View v) {
    	//Log.d(TAG, "pause button clicked");
    	m_stopwatchService.pause();
    	
    	showStartResetButtons();
    }
    
    public void onResetClicked(View v) {
    	//Log.d(TAG, "reset button clicked");
    	m_stopwatchService.reset();
    	
    	m_lapList.clear();
    }
    
    public void onLapClicked(View v) {
    	//Log.d(TAG, "lap button clicked");
    	m_stopwatchService.lap();
    	
    	//m_lapList.insert(m_stopwatchService.getFormattedElapsedTime(), 0);
		m_lapList.add(String.valueOf(m_lapList.getCount()+1)+".  "+m_stopwatchService.getFormattedElapsedTime()); 
    }*/
    
    public void updateElapsedTime() {
    	if ( m_stopwatchService != null )
    		m_elapsedTime.setText(m_stopwatchService.getFormattedElapsedTime());
    }
    
}
