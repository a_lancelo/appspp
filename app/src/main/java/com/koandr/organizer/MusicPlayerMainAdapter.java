package com.koandr.organizer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MusicPlayerMainAdapter extends BaseAdapter {
	
	Context context;
	String [] names, artists;
	
	MusicPlayerMainAdapter(Context context, String [] names, String [] artists) {
       this.context = context;
       this.names = names;
       this.artists = artists;
    }
	
	@Override
	public int getCount() {
	   return names.length;
	}
	
	@Override
	public Object getItem(int position) {
	   return null;
	}
	
	@Override
	public long getItemId(int position) {
	   return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	   View view = (View) ((AppMainActivity) context).getLayoutInflater().inflate(R.layout.music_player_item, parent, false);
	   ((TextView) view.findViewById(R.id.name)).setText(names[position]);
	   ((TextView) view.findViewById(R.id.artist)).setText(artists[position]);
	   return view;
	}
	
}
