package com.koandr.organizer;

import java.io.File;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;

public class FileManagerRenamerDialog extends DialogFragment {

	EditText name;

	public Dialog onCreateDialog(Bundle savedInstanceState) {				
		name = new EditText(getActivity());
		name.setText(new File(getArguments().getString("path")).getName());			
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
			.setTitle(R.string.rename)
			.setView(name)
			.setNeutralButton(R.string.cancel, listener)
			.setPositiveButton(R.string.ok, listener);
		return adb.create();
	}

	DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int id) {						
			if (id == -1) {
				Intent data = new Intent();
				data.putExtra("path", getArguments().getString("path"));
				data.putExtra("name", name.getText().toString());
				getTargetFragment().onActivityResult(getTargetRequestCode(), 1, data);
			}		
			dismiss();       
		}

    };

}
