package com.koandr.organizer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Andrew on 9/26/14.
 */
public class NotesAdapter extends BaseAdapter {

    Context context;
    ArrayList<NotesCard> cards;

    NotesAdapter(Context context, ArrayList<NotesCard> cards) {
        this.context = context;
        this.cards = cards;
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = (View) ((AppMainActivity) context).getLayoutInflater().inflate(R.layout.notes_cell, parent, false);
        ((TextView) view.findViewById(R.id.text)).setText(cards.get(position).getText());
        ((TextView) view.findViewById(R.id.date)).setText(cards.get(position).getDateExpanded(context));
        return view;
    }

}
