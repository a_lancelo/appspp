package com.koandr.organizer;

import java.net.*;
import java.util.*;

public class FTPServerTcpListener extends Thread {

	ServerSocket listenSocket;
	FTPServerSessionThread sessionThread;
	ArrayList<FTPServerSessionThread> sessionThreads = new ArrayList<FTPServerSessionThread>();	

	public FTPServerTcpListener(ServerSocket listenSocket) {
		this.listenSocket = listenSocket;
	}

	@Override
	public void run() {			
		try {				
			while (true) {
				sessionThread = new FTPServerSessionThread(listenSocket.accept());
				sessionThread.start();
				sessionThreads.add(sessionThread);
			}				
		} catch (Exception e) {

		}
	}	

	private void terminateAllSessions() {
		synchronized (this) {
			for (FTPServerSessionThread sessionThread : sessionThreads) {
				if (sessionThread != null) {
					sessionThread.closeDataSocket();
					sessionThread.closeSocket();
				}
			}
		}
	}

	public void quit() {
		terminateAllSessions();
		try {
			listenSocket.close(); 
		} catch (Exception e) {

		}
	}

}
