package com.koandr.organizer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.widget.ImageView;

public class FileManagerDrawableLoader {

    private Activity mActivity;
 
	 public FileManagerDrawableLoader(Context context){
       //Make the background thead low priority. This way it will not affect the UI performance
       photoLoaderThread.setPriority(Thread.NORM_PRIORITY-1);
   }
    final int stub_id =R.drawable.file_manager_apk;//	apk_file;

  public void displayImage(String url, Activity activity, ImageView imageView)
   {
	   mActivity=activity;
         queuePhoto(url, activity, imageView);
           imageView.setImageResource(stub_id);
    }
 
 private void queuePhoto(String url, Activity activity, ImageView imageView)
   {
       //This ImageView may be used for other images before. So there may be some old tasks in the queue. We need to discard them. 
       photosQueue.Clean(imageView);
       PhotoToLoad p=new PhotoToLoad(url, imageView);
       synchronized(photosQueue.photosToLoad){
           photosQueue.photosToLoad.push(p);
           photosQueue.photosToLoad.notifyAll();
       }

       //start thread if it's not started yet
       if(photoLoaderThread.getState()==Thread.State.NEW)
           photoLoaderThread.start();
   }

  
   private Drawable getDrawable(String url) 
   {
        Drawable drawable = new FileManagerFileUtils(mActivity).getapkicon(url);
	   return drawable;
   }

  
   private class PhotoToLoad
   {

	   public String url;
	   public ImageView imageView;

       public PhotoToLoad(String u, ImageView i){
           url=u; 
           imageView=i;
       }
   }

   /** The photos queue. */
   PhotosQueue photosQueue=new PhotosQueue();

   /**
    * Stop thread.
    */
   public void stopThread()
   {
       photoLoaderThread.interrupt();
   }

   //stores list of photos to download
   /**
    * The Class PhotosQueue.
    */
   class PhotosQueue
   {

       /** The photos to load. */
       private Stack<PhotoToLoad> photosToLoad=new Stack<PhotoToLoad>();

       //removes all instances of this ImageView
       /**
        * Clean.
        *
        * @param image the image
        */
       public void Clean(ImageView image)
       {
           for(int j=0 ;j<photosToLoad.size();){
               if(photosToLoad.get(j).imageView==image)
                   photosToLoad.remove(j);
               else
                   ++j;
           }
       }
   }

   /**
    * The Class PhotosLoader.
    */
   class PhotosLoader extends Thread {

       /* (non-Javadoc)
        * @see java.lang.Thread#run()
        */
       public void run() {
           try {
               while(true)
               {
                   //thread waits until there are any images to load in the queue
                   if(photosQueue.photosToLoad.size()==0)
                       synchronized(photosQueue.photosToLoad){
                           photosQueue.photosToLoad.wait();
                       }
                   if(photosQueue.photosToLoad.size()!=0)
                   {
                       PhotoToLoad photoToLoad;
                       synchronized(photosQueue.photosToLoad){
                           photoToLoad=photosQueue.photosToLoad.pop();
                       }
                       Drawable bmp=getDrawable(photoToLoad.url);
                     /*  if(cachefiles){
                    	  // CreateCacheFile( ((BitmapDrawable)bmp).getBitmap(),photoToLoad.url);
                       }
                       cache.put(photoToLoad.url, bmp);*/
                      // if(((String)photoToLoad.imageView.getTag()).equals(photoToLoad.url)){
                           DrawableDisplayer bd=new DrawableDisplayer(bmp, photoToLoad.imageView);
                           Activity a=(Activity)photoToLoad.imageView.getContext();
                           a.runOnUiThread(bd);
                     //  }
                   }
                   if(Thread.interrupted())
                       break;
               }
           } catch (InterruptedException e) {
               //allow thread to exit
           }
       }
   }

   PhotosLoader photoLoaderThread=new PhotosLoader();

    class DrawableDisplayer implements Runnable
   {

       Drawable bitmap;
       ImageView imageView;

	   public DrawableDisplayer(Drawable b, ImageView i) {
		  bitmap=b;
		  imageView=i;
		}

	   public void run()
       {
           if(bitmap!=null)
               imageView.setImageDrawable(bitmap);
           else
             imageView.setImageResource(stub_id);
       }
   }

    public static void copyStream(InputStream is, OutputStream os) {
           final int buffer_size=1024;
           try
           {
               byte[] bytes=new byte[buffer_size];
               for(;;)
               {
                 int count=is.read(bytes, 0, buffer_size);
                 if(count==-1)
                     break;
                 os.write(bytes, 0, count);
               }
           }
           catch(Exception ex){}
       }


}
