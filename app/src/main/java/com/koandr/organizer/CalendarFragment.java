package com.koandr.organizer;

import java.util.Calendar;
import java.util.Locale;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class CalendarFragment extends Fragment {
	
	private LinearLayout field;
 	private int curMonth = 0, curYear = 0, 
 			dayOfWeekOfFirstDayOfMonth = 0, dayOfWeekOfLastDayOfMonth = 0,
 			dayCurrent, monthCurrent, yearCurrent;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.calendar_main2, container, false);
       
    	
    	/*ScrollView scrollView = new ScrollView(getActivity());
        scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        
        LinearLayout ll = new LinearLayout(getActivity());
        field = new LinearLayout(getActivity());
        field.setOrientation(LinearLayout.VERTICAL);
        //LinearLayout.LayoutParams lParamsL = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT,0.1f);
        //field.setLayoutParams(layoutParams);       
        field.setGravity(Gravity.CENTER_HORIZONTAL);
    	LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
    	layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
    	field.setLayoutParams(layoutParams);
    	ll.setLayoutParams(layoutParams);
    	ll.addView(field);
        scrollView.addView(ll);*/
        
        field = (LinearLayout) rootView.findViewById(R.id.linearLayout);	
		movement(0);
		setHasOptionsMenu(true);
        return rootView;
    }
   	
   	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.calendar_menu, menu);
	    super.onCreateOptionsMenu(menu,inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_item_1:
				movement(-1);
				break;
			case R.id.action_item_2:
				movement(1);
				break;
		}
	    return super.onOptionsItemSelected(item);
	}
	
	private void movement(int moving) {
		
		int maxDaysInMonth = 0;
		int today = 0;
		int dayOfWeek = 0;
		
		/*String NameOfMonth[] = { getString(R.string.January), getString(R.string.February),
			getString(R.string.March), getString(R.string.April),
			getString(R.string.May), 	getString(R.string.June),
			getString(R.string.Jule), getString(R.string.August),
			getString(R.string.September), 	getString(R.string.October),
			getString(R.string.November), 	getString(R.string.December) };
		*/
		
		TypedArray nameMonth = getResources().obtainTypedArray(R.array.months);
		
		Calendar calendar = Calendar.getInstance();
		
		switch (moving) {
			case 0:
				curMonth = calendar.get(Calendar.MONTH)+1;
				curYear = calendar.get(Calendar.YEAR);
				//int 
				today = calendar.get(Calendar.DATE);
				//int 
				dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)-1;	
				dayCurrent = today;
				monthCurrent = curMonth;
				yearCurrent = curYear;
				break;
			case 1:
				curMonth++;
			 	today = 1;
				dayOfWeek=dayOfWeekOfLastDayOfMonth+1; 
				break;
			case -1:
				curMonth--;
				dayOfWeek=dayOfWeekOfFirstDayOfMonth-1;
				break;
		} 
			
			
		if (curMonth == 13) {
			curMonth=1;
			curYear++;
		}
		
		if (curMonth==0) {
			curMonth=12;
			curYear--;
		}
			 
			 
		switch (curMonth) {
			case 2:
				if (curYear%4 == 0) {
					maxDaysInMonth = 29;
				} else {
					maxDaysInMonth = 28;
				}
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				maxDaysInMonth = 30;
				break;
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				maxDaysInMonth = 31;
				break;
		}
		
		if (moving == -1) { 
			today = maxDaysInMonth;
		}
		if (dayOfWeek == 0) {
			dayOfWeek = 7;
		}
		if (today%7-dayOfWeek >= 0) {
			dayOfWeekOfFirstDayOfMonth = 7-(today%7-dayOfWeek)+1;
		} else {
			dayOfWeekOfFirstDayOfMonth = -1*(today%7-dayOfWeek)+1;
		}
		dayOfWeekOfLastDayOfMonth = (dayOfWeekOfFirstDayOfMonth+ maxDaysInMonth%7-1)%7;
        if (dayOfWeekOfFirstDayOfMonth == 8) {
            dayOfWeekOfFirstDayOfMonth = 1;
        }
        if (dayOfWeekOfLastDayOfMonth == 8) {
            dayOfWeekOfLastDayOfMonth = 1;
        }
		if (!Locale.getDefault().getISO3Language().equals("rus")&moving == 0){
			dayOfWeekOfFirstDayOfMonth++;
			dayOfWeekOfLastDayOfMonth++;
			if (dayOfWeekOfFirstDayOfMonth == 8) { 
				dayOfWeekOfFirstDayOfMonth = 1;
			}
			if (dayOfWeekOfLastDayOfMonth == 8) {
				dayOfWeekOfLastDayOfMonth = 1;
			}
		}
	    getActivity().setTitle(nameMonth.getString(curMonth-1)+" "+String.valueOf(curYear));
	    //+" "+String.valueOf(dayOfWeekOfFirstDayOfMonth)+" "
	    		//+String.valueOf(dayOfWeekOfLastDayOfMonth));
	    nameMonth.recycle();		
	    
		int CurrentDayOfWeekInI=0;
		
		field.removeAllViews();
		
		int k = 0;
		LinearLayout row = null;
		
		
		for (int i = 0; i < 7; i++) {
			TypedArray ids = getResources().obtainTypedArray(R.array.days_of_week);
			TextView textView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.calendar_main_text_view, field, false); //new Button(getActivity(), null, R.style.button);
			textView.setText(ids.getString(i));
			ids.recycle();			
			switch (k) {
				case 0:
					row = new LinearLayout(getActivity());
			        row.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					row.setOrientation(LinearLayout.HORIZONTAL);
					row.addView(textView);
					k++;
					break;
				default: 
					row.addView(textView);
					k++;
					if (k == 7) {
						field.addView(row);//, field.getLayoutParams());
						k = 0;
					}
					break;
			}
		}
		
		
		for (int i = 1; i < 43; i++) {
			View view = getActivity().getLayoutInflater().inflate(R.layout.calendar_main_button, field, false); //new Button(getActivity(), null, R.style.button);
			Button btnNew = (Button) view.findViewById(R.id.button);
			//btnNew.setText("  "+"  ");
			if (i > 0 & i < 9 & CurrentDayOfWeekInI == 0 & i == dayOfWeekOfFirstDayOfMonth) {
				CurrentDayOfWeekInI=1;
			}
			if (CurrentDayOfWeekInI != 0) {
			//	btnNew.setBackgroundColor(-1179716);
				if (curYear == yearCurrent & curMonth == monthCurrent & CurrentDayOfWeekInI == dayCurrent) {
					btnNew.setTypeface(null, Typeface.BOLD);
					btnNew.setTextColor(Color.RED);
				}
				//if (CurrentDayOfWeekInI > 9) {
				btnNew.setText(String.valueOf(CurrentDayOfWeekInI));
				/*} else {
		        	btnNew.setText(" "+String.valueOf(CurrentDayOfWeekInI)+" ");
				}*/
			 	//btnNew.setId(CurrentDayOfWeekInI);
				CurrentDayOfWeekInI++;
			}
			if (CurrentDayOfWeekInI-1 == maxDaysInMonth){
				CurrentDayOfWeekInI=0;
			}
			
			switch (k) {
				case 0:
					row = new LinearLayout(getActivity());
			        row.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					row.setOrientation(LinearLayout.HORIZONTAL);
					row.addView(view);
					k++;
					break;
				default: 
					row.addView(view);
					k++;
					if (k == 7) {
						field.addView(row);//, field.getLayoutParams());
						k = 0;
					}
					break;
			}
	
		}
	}

}
