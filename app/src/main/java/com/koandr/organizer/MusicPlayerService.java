package com.koandr.organizer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.media.*;
import java.io.*;
import android.media.MediaPlayer.OnCompletionListener;
import java.util.List;
import java.util.ArrayList;

public class MusicPlayerService extends Service {
	//private static final String TAG = "StopwatchService";
	private static final int NOTIFICATION_ID = 1;
	
	public class LocalBinder extends Binder {
		MusicPlayerService getService() {
			return MusicPlayerService.this;
		}
	}
	
	//private String songName;
	private int position = 0, progress = 0;//-1;
	//private static final String MEDIA_PATH = new String("/sdcard/");
	private List<String> songs = new ArrayList<String>();
	private List<String> artists = new ArrayList<String>();
	//private List<String> songs2 = new ArrayList<String>();
	
	private MusicPlayer musicPlayer;
	private LocalBinder m_binder = new LocalBinder();
	private NotificationManager m_notificationMgr;
	private Notification m_notification;

	// Timer to update the ongoing notification
    private final long mFrequency = 100;  // milliseconds
    private final int TICK_WHAT = 2; 
	private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
        	updateNotification();
        	sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };
	
	@Override
	public IBinder onBind(Intent intent) {
		//Log.d(TAG, "bound");
		
		return m_binder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		//Log.d(TAG, "created");
		
		//Text
			//StopwatchActivity sa = new StopwatchActivity();
			//playSong(sa.nameSong, false);
		//BindAllSongs();
		//
		musicPlayer = new MusicPlayer();
		m_notificationMgr = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		
		createNotification();
	}
	
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.i("LocalService", "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
		//BindAllSongs();
        return START_STICKY;
    }
	
    public void createNotification() {
    	//Log.d(TAG, "creating notification");

		//BindAllSongs();
		
    	int icon = R.drawable.ic5;//slidingmenu_item_1; //R.drawable.icon;
    	CharSequence tickerText = "Apps++";//"Stopwatch";
    	long when = System.currentTimeMillis();

    	m_notification = new Notification(icon, tickerText, when);
    	m_notification.flags |= Notification.FLAG_ONGOING_EVENT;
    	m_notification.flags |= Notification.FLAG_NO_CLEAR;
    }
    
    public void updateNotification() {
    	// Log.d(TAG, "updating notification");

    	Context context = getApplicationContext();
    	CharSequence contentTitle = getSongName();//"Stopwatch";
    	CharSequence contentText = "Apps++ "+getString(R.string.music);;//getFormattedElapsedTime();

    	Intent notificationIntent = new Intent(this, AppMainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//Intent.FLAG_ACTIVITY_NEW_TASK);
    	PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 
		PendingIntent.FLAG_CANCEL_CURRENT);  //Intent.FLAG_ACTIVITY_CLEAR_TOP);  //0);
		//contentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
    	// the next two lines initialize the Notification, using the configurations above
    	m_notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
    	//savePreferences();
		m_notificationMgr.notify(NOTIFICATION_ID, m_notification);
    }
    
    /*public static String MY_PREF = "MY_PREF";
    public void savePreferences() 
	{
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getSharedPreferences(MY_PREF, mode); 	
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("firstFragmentV10", 1);
		editor.commit();
	}*/
    
    public void showNotification() {
    	//Log.d(TAG, "showing notification");
    	
		
		//Text
		//StopwatchActivity sa = new StopwatchActivity();
		//p = sa.p;
		//playSong(sa.nameSong, false);
		
	/*	if (mp!=null) {
			//mp.start();
			mp.seekTo((int) getStateDuraion());
		}	else*/
		playSong(songs.get(position), false);
		//
		
    	updateNotification();    	
        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
		
		
    }
    
    public void hideNotification() {
    	//Log.d(TAG, "removing notification");
		//Text
		mp.stop();
		//
    	m_notificationMgr.cancel(NOTIFICATION_ID);
    	mHandler.removeMessages(TICK_WHAT);
    }
    
	public void start() {
		//Log.d(TAG, "start");
		musicPlayer.start();
		
		showNotification();
	}
	
	public void pause() {
		//Log.d(TAG, "pause");
		musicPlayer.pause();
		
		hideNotification();
	}
	
	public void lap() {
		//Log.d(TAG, "lap");
		musicPlayer.lap();
	}
	
	public void reset() {
		//Log.d(TAG, "reset");
		if (mp!=null)
		mp.reset();
		musicPlayer.reset();
	}
	
	public long getElapsedTime() {
		return musicPlayer.getElapsedTime();
	}
	
	public String getFormattedElapsedTime() {
		return formatElapsedTime(getElapsedTime());
	}
	
	public boolean isStopwatchRunning() {
		return musicPlayer.isRunning();
	}
	
	/***
	 * Given the time elapsed in tenths of seconds, returns the string
	 * representation of that time. 
	 * 
	 * @param now, the current time in tenths of seconds
	 * @return 	String with the current time in the format MM:SS.T or 
	 * 			HH:MM:SS.T, depending on elapsed time.
	 */
	private String formatElapsedTime(long now) {
		long hours=0, minutes=0, seconds=0, tenths=0;
		StringBuilder sb = new StringBuilder();
		
		if (now < 1000) {
			tenths = now / 100;
		} else if (now < 60000) {
			seconds = now / 1000;
			now -= seconds * 1000;
			tenths = (now / 100);
		} else if (now < 3600000) {
			hours = now / 3600000;
			now -= hours * 3600000;
			minutes = now / 60000;
			now -= minutes * 60000;
			seconds = now / 1000;
			now -= seconds * 1000;
			tenths = (now / 100);
		}
		
		if (hours > 0) {
			sb.append(hours).append(":")
				.append(formatDigits(minutes)).append(":")
				.append(formatDigits(seconds));//.append(".")
				//.append(tenths);
		} else {
			sb.append(formatDigits(minutes)).append(":")
			.append(formatDigits(seconds));//.append(".")
			//.append(tenths);
		}
		
		return sb.toString();
	}
	
	private String formatDigits(long num) {
		return (num < 10) ? "0" + num : new Long(num).toString();
	}

	
	MediaPlayer mp;
	
	private void playSong(String songPath, boolean recovery) {
		
		//MediaPlayer 
		mp = new MediaPlayer();
		
		try {
			mp.reset();
			mp.setDataSource(songPath);
			mp.prepare();
			mp.start();
			if (progress > mp.getDuration()) {
				progress = 0;
			}	
			musicPlayer.setTimeProgress(progress);
			mp.seekTo(progress);
			
			progress = 0;
			
			/*if (recovery){
				mp.seekTo(v);
				MPSB.setProgress(v);
			} else v=0;
			//setTitle(String.valueOf(mp.getDuration()));
			MPSB.setMax(mp.getDuration());

			st="";
			m2= (mp.getDuration()/1000)/60;
			s2= (mp.getDuration()/1000)%60;
			//h2= m2/60;
			//m2= m2%60;
			//if (h2<10) st="0";
			//st=st+Long.toString( h2)+":";
			if (m2<10) st="0";
			st=st+Long.toString( m2)+ ":";
			if (s2<10) st=st+"0";
			st=st+Long.toString( s2);
			Duration.setText(st);
			//
			//if (recovery)
			//v=0;
			showTimer(mp.getDuration());

			Play=true;
*/

// Setup listener so next song starts automatically
			mp.setOnCompletionListener(new OnCompletionListener() {
					public void onCompletion(MediaPlayer arg0) {
						nextSing();
					}
				});
		} catch (IOException e) {
			//	Log.v(getString(R.string.app_name), e.getMessage());
		}
	}
	
	public boolean isPlaySong() {
		if (mp!=null) return true; 
			else return false;
	}
	
	public String getFormatTime(int time){
		return formatElapsedTime(time);
	}
	
	public String getDuration(){
		return formatElapsedTime(Long.parseLong(String.valueOf(mp.getDuration())));
	}
	
	public int getIntDuration(){
		if (mp!=null) return mp.getDuration();
		else return 0;
	}
	
	public long getStateDuration(){
		return musicPlayer.getElapsedTime();
	}
	
	public String getSongName(){
		return songs.get(position).substring(songs.get(position).lastIndexOf("/")+1, songs.get(position).length()-4);	
	}
	
	public String getArtist() {
		return artists.get(position);
	}
	
	public void setProgress(int progress) {
		this.progress = progress;
		//musicPlayer.setTimeProgress(time);
		//mp.seekTo(time);
	}
	
	public void setSeekBarProgress(int time) {
		if (musicPlayer != null && mp != null) {
			musicPlayer.setTimeProgress(time);
			mp.seekTo(time);
		}
	}
	
	/*Cursor cursor;
	public void BindAllSongs() {        
		/** Making custom drawable ***
		String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
		final String[] projection = new String[] {
			MediaStore.Audio.Media.DISPLAY_NAME,
			MediaStore.Audio.Media.ARTIST,
			MediaStore.Audio.Media.DATA};
		final String sortOrder = MediaStore.Audio.AudioColumns.TITLE
			+ " COLLATE LOCALIZED ASC";

		try {
			// the uri of the table that we want to query
			Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
			// query the db
			cursor = getBaseContext().getContentResolver().query(uri,
																 projection, selection, null, sortOrder);
			if (cursor != null) {
				//songs = new ArrayList<genericSongClass>(cursor.getCount());
				cursor.moveToFirst();                       
				while (!cursor.isAfterLast()) { 
					/*GSC = new genericSongClass();
					 GSC.songTitle = cursor.getString(0);
					 GSC.songArtist = cursor.getString(1);   
					 GSC.songData = cursor.getString(2);***
					songs.add(cursor.getString(2));
					songs2.add(cursor.getString(0));
					cursor.moveToNext();
				}
			} } catch (Exception ex) {

		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}       

	}*/
	
	public void nextSing(){
		musicPlayer.reset();
		musicPlayer.start();
		if (mp!=null) mp.reset();
		if (position+1 == songs.size()) position=0; else position++;
		playSong(songs.get(position),false);
	}

	public void previousSing(){
		musicPlayer.reset();
		musicPlayer.start();
		if (mp!=null) mp.reset();
		if (position==0||position==-1) position = songs.size()-1; else position--;
		playSong(songs.get(position),false);
	}
	
	public void setListSong(List<String> allSongs) {
		songs = allSongs;
	}
	
	public void setListArtists(List<String> artists) {
		this.artists = artists;
	}
	
	public void setPosition(int position) {
		this.position = position;
	}
	
	public int getPosition() {
		return position;
	}
}
