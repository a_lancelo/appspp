package com.koandr.organizer;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Andrew on 9/26/14.
 */
public class NotesEditorFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.notes);
        View rootView = inflater.inflate(R.layout.notes_editor, container, false);
        int position = getActivity().getIntent().getIntExtra("note", 0);
        NotesCard card;
        if (position != -1) {
            card = new NotesStorage().getCards(getActivity()).get(position);
            ((EditText) rootView.findViewById(R.id.editText)).setText(card.getText());
        } else {
            card = new NotesCard();
            card.setDate(getDateNow());
        }
        ((TextView) rootView.findViewById(R.id.date)).setText(card.getDateExpanded(getActivity()));
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (!prefs.getBoolean("t9", false)) {
            ((EditText) rootView.findViewById(R.id.editText))
                    //.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

            .setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            ((EditText) rootView.findViewById(R.id.editText)).setSingleLine(false);
        }
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.notes_editor, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            NotesCard card = new NotesCard();
            card.setDate(getDateNow());
            card.setText(((EditText) getView().findViewById(R.id.editText)).getText().toString());
            ArrayList<NotesCard> cards = new NotesStorage().getCards(getActivity());
            int position = getActivity().getIntent().getIntExtra("note", 0);
            if (position != -1) {
                cards.set(position, card);
            } else {
                cards.add(card);
            }
            new NotesStorage().setCards(getActivity(), cards);
            getActivity().setResult(1);
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private String getDateNow() {
        Calendar calendar = Calendar.getInstance();
        String date = String.valueOf(calendar.get(Calendar.YEAR)) + "." +
            String.valueOf(calendar.get(Calendar.MONTH)+1) + "." +
            String.valueOf(calendar.get(Calendar.DATE)) + " " +
            String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)) + ":";
        String minute = String.valueOf(calendar.get(Calendar.MINUTE));
        if (minute.length() == 1) {
            minute = "0" + minute;
        }
        date += minute;
        return date;
    }

}
