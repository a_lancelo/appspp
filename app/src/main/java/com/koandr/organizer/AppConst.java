package com.koandr.organizer;


public class AppConst {

    //Sliding menu
    static final int FileManager = 0;
	static final int NewsRSS = 1;
	static final int Music = 2;
	static final int Notes = 3;
	static final int Calendar = 4;
	static final int TextEditor = 5;
	static final int Stopwatch = 6;
    static final int FTPServer = 7;
	static final int Preferences = 8;

    //Secondary screens
	static final int NewsRSSBookmarks = 110;
	static final int NewsRSSBookmarksEditor = 111;
	static final int NewsRSSBrowser = 112;
	static final int NotesViewer = 140;
    static final int NotesEditor = 141;

	static final String PreferencesFile = "preferences";
	static final String NewsRSSFile = "newsRSS";
	
}
