package com.koandr.organizer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class NewsRSSCard {
	
	private String siteTitle = "", title = "", text = "", URL = "", date = "";
	private Date dateFormated;
	
	public void setSiteTitle(String siteTitle) {
		this.siteTitle = siteTitle;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public void setURL(String URL) {
		this.URL = URL;
	}
	
	@SuppressWarnings("deprecation")
	public void setDate(String dateString) {
		SimpleDateFormat format1 = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss", Locale.ENGLISH);
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH);
		Date date = null;
		try {
			date = format1.parse(dateString);
		} catch (ParseException ex1) {
			try {
				date = format2.parse(dateString);
			} catch (ParseException ex2) {
				
			}
		}
		if (date != null) {
			dateFormated = date;
			this.date = date.toLocaleString();
		} else { 
			this.date = dateString;
		}
	}
	
	public String getSiteTitle() {
		return siteTitle;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getText() {
		return text;
	}

	public String getURL() {
		return URL;
	}
	
	public String getDate() {
		return date;
	}
	
	public Date getDateFormated() {
		return dateFormated;
	}
	
}
