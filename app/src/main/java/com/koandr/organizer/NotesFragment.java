package com.koandr.organizer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class NotesFragment extends Fragment {

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		getActivity().setTitle(R.string.notes);
        View rootView = inflater.inflate(R.layout.notes, container, false);
        ListView listView = ((ListView) rootView.findViewById(R.id.listView));
        listView.setAdapter(new NotesAdapter(getActivity(), new NotesStorage().getCards(getActivity())));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), AppMainActivity.class);
                intent.putExtra("screen", AppConst.NotesViewer);
                intent.putExtra("note", i);
                startActivityForResult(intent, 1);
            }
        });
		setHasOptionsMenu(true);
        return rootView;
    }
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.notes, menu);
	    super.onCreateOptionsMenu(menu,inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_add) {
			Intent intent = new Intent(getActivity(), AppMainActivity.class);
			intent.putExtra("screen", AppConst.NotesEditor);
			intent.putExtra("note", -1);
			startActivityForResult(intent, 1);
		}
	    return super.onOptionsItemSelected(item);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		((AppMainActivity) getActivity()).onSelectItem(resultCode, data);
        ((ListView) getView().findViewById(R.id.listView)).setAdapter(new NotesAdapter(getActivity(), new NotesStorage().getCards(getActivity())));
	}

}
