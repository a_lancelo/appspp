package com.koandr.organizer;

import android.os.Bundle;

public class PreferencesFragment extends PreferenceFragment {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
    	getActivity().setTitle(R.string.preferences);
        addPreferencesFromResource(R.xml.preferences);
    }
	
}
