package com.koandr.organizer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


public class NotesEditFragment extends Fragment {
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	getActivity().setTitle(R.string.notes);
        View rootView = inflater.inflate(R.layout.notes_edit, container, false);
        
        numberOfNote = //getArguments()
        		getActivity().getIntent().getIntExtra("note", -1);
		//this.getArguments().getInt("note");

		//setContentView(R.layout.notes_edit);
		title = (EditText) rootView.findViewById(R.id.notes_edit_title);
		note = (EditText) rootView.findViewById(R.id.notes_edit_note);
		PFopenFile("MyNotes");
		
		if (numberOfNote != -1) {
			title.setText(titles.get(numberOfNote));
			note.setText(notes.get(numberOfNote));
		}
        
        
        setHasOptionsMenu(true);
        return rootView;
    }
	
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.notes_menu_edit, menu);
	    super.onCreateOptionsMenu(menu,inflater);
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    //Log.d("MENU", "Cliced MenuItem is " + item.getTitle());
		if (item.getItemId()==R.id.action_item_2){
			if (numberOfNote == -1) {
				targetWas.add(1);
				titles.add(title.getText().toString());
				notes.add(note.getText().toString());
			} else {
				targetWas.set(numberOfNote, 1);
				titles.set(numberOfNote, title.getText().toString());
				notes.set(numberOfNote, note.getText().toString());
			}
			
			PFsaveFile("MyNotes");
			
			//Fragment fragment=new NotesFragment();
			//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
			//finish();
			//FragmentManager fragmentManager = getFragmentManager();
				//fragmentManager.popBackStack();
			getActivity().setResult(1);
			getActivity().finish();
		}
		
		if (item.getItemId()==R.id.action_item_1){
			if (numberOfNote == -1) {
				targetWas.add(0);
				titles.add(title.getText().toString());
				notes.add(note.getText().toString());
			} else {
				targetWas.set(numberOfNote, 0);
				titles.set(numberOfNote, title.getText().toString());
				notes.set(numberOfNote, note.getText().toString());
			}
			
			PFsaveFile("MyNotes");
			
			//Fragment fragment=new NotesFragment();
			//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
			//finish();
			//FragmentManager fragmentManager = getFragmentManager();
			//fragmentManager.popBackStack();
			getActivity().setResult(1);
			getActivity().finish();
			//show(fragment);
			//remove(this).commit();
		}
		
		if (item.getItemId()==R.id.action_item_3){
			if (numberOfNote!=-1){
				targetWas.remove(numberOfNote);
				titles.remove(numberOfNote);
				notes.remove(numberOfNote);
				
				PFsaveFile("MyNotes");
				//Fragment fragment=new NotesFragment();
				//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
				//FragmentManager fragmentManager = getFragmentManager();
				//fragmentManager.popBackStack();
				getActivity().setResult(1);
				getActivity().finish();
				//finish();
			} else {
				
				
				
				/*WifiManager wifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
				WifiInfo wInfo = wifiManager.getConnectionInfo();
				String macAddress = wInfo.getMacAddress(); 
				
				if (note.getText().toString().equals("a")){
					//public static 
					String MY_PREF = "MY_PREF";
					int mode = Activity.MODE_PRIVATE;
					SharedPreferences mySharedPreferences = getActivity().getSharedPreferences(MY_PREF, mode); 
				    SharedPreferences.Editor editor = mySharedPreferences.edit();
					editor.putString("aaa", macAddress);
					editor.commit();				
				}*/
				
				//FragmentManager fragmentManager = getFragmentManager();
				//fragmentManager.popBackStack();
				getActivity().setResult(1);
				getActivity().finish();
				//finish();
			}
		}
	    return super.onOptionsItemSelected(item);
	}
	
	/*public void onBackPressed(){
		FragmentManager fm=getFragmentManager();
		FragmentTransaction ft=fm.beginTransaction();
		ft.addToBackStack(null);
		fm.popBackStack();
		
		//FragmentManager fm = getActivity().getSupportFragmentManager();
	    //fm.popBackStack();
		//Fragment fragment=new NotesFragment();
		//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
    }*/
	
	//String Title;

	String name; // name of Notebook
	
	int numberOfNote;
	ArrayList<String> notes = new ArrayList<String>();
	ArrayList<String> titles = new ArrayList<String>();
	ArrayList<Integer> targetWas = new ArrayList<Integer>();
	EditText title, note;
	
	private void PFopenFile(String fileName) {
		try {
			InputStream inputstream = getActivity().openFileInput(fileName+".txt");
			if (inputstream != null) {
				InputStreamReader isr = new InputStreamReader(inputstream);
				BufferedReader reader = new BufferedReader(isr);
				String str;
				String FSt="";
				StringBuffer buffer = new StringBuffer();
				int i = -1; 
				boolean nowWillTitle=true;
				boolean nowWillName=true;
				
				while ((str = reader.readLine()) != null) {
					buffer.append(str + "\n");
						if (str.equals("com.koandr.organizer")==false){
							if (nowWillName) {
								name=str;
								nowWillName=false;
							}	else {
								if (nowWillTitle){
									//Title=str;
									//titles.add(str);
									targetWas.add(Integer.parseInt(str.substring(0,1)));
									titles.add(str.substring(1));
									nowWillTitle=false;
								} else{
									if (FSt.length()!=0)
										FSt=FSt+"\n"+str; else
											FSt=str;
									//FSt=FSt+str+"\n";
									notes.set(i,FSt);
								}
							}
							
							}else{
								notes.add("");
								i++;
								FSt="";
								nowWillTitle=true;
							}
				}
				inputstream.close();
			}
		} catch (Throwable t) {}
	}

	private void PFsaveFile(String FileName) {
		try {
			OutputStream outputstream = getActivity().openFileOutput(FileName+".txt", 0);
			OutputStreamWriter osw = new OutputStreamWriter(outputstream);
			if (titles.size() == 1) osw.write("Notebook\n"); else 
				if (titles.size() != 0)
					osw.write(name+"\n");
			for (int i=0; i<notes.size(); i++)
				osw.write("com.koandr.organizer\n"+String.valueOf(targetWas.get(i))+titles.get(i)+"\n"+notes.get(i)+"\n");
			osw.close();
		} catch (Throwable t) {}
	}
	
}
