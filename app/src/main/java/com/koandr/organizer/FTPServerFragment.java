package com.koandr.organizer;

import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.net.*;
import android.net.wifi.*;
import android.text.format.Formatter;
import android.support.v4.app.Fragment;

public class FTPServerFragment extends Fragment {
	
	private final long mFrequency = 100;
    private final int TICK_WHAT = 2; 
	private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
        	updateElapsedTime();
        	sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

  	private FTPServerService ftpServerService;
	private ServiceConnection m_stopwatchServiceConn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			ftpServerService = ((FTPServerService.LocalBinder)service).getService();
			if (ftpServerService.isRunning()) {								
				switchButton.setChecked(true);
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			ftpServerService = null;
		}
	};

	private void bindStopwatchService() {
        getActivity().bindService(new Intent(getActivity(), FTPServerService.class), 
								  m_stopwatchServiceConn, Context.BIND_AUTO_CREATE);
	}

	private void unbindStopwatchService() {
		if (ftpServerService != null) {
			getActivity().unbindService(m_stopwatchServiceConn);
		}
	}
	
	Switch switchButton;
	TextView address;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
    	getActivity().setTitle(R.string.ftp_server);
		View rootView = inflater.inflate(R.layout.ftp_server, container, false);
		address = (TextView) rootView.findViewById(R.id.address);
		switchButton = (Switch) rootView.findViewById(R.id.switchButton);
		switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (ftpServerService != null) {
						if (isChecked) {
							ftpServerService.start();												
						} else {
							ftpServerService.pause();
						}
					}
				}	

			});		
		getActivity().startService(new Intent(getActivity(), FTPServerService.class));
        bindStopwatchService();     
		mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
        return rootView;
    }

    @Override
	public void onDestroy() {
        super.onDestroy();
        unbindStopwatchService();
    }

    public void updateElapsedTime() {
        if (getActivity() != null) {
            if (ftpServerService != null) {
                if (!isOnline() & switchButton.getVisibility() == View.VISIBLE) {
                    switchButton.setVisibility(View.GONE);
                    address.setText(getString(R.string.wifi_off));
                    //if (ftpServerService != null) {
                    ftpServerService.hideNotification();
                    //}
                }
                if (isOnline() & switchButton.getVisibility() == View.GONE) {
                    switchButton.setVisibility(View.VISIBLE);
                    WifiManager wm = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
                    String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                    address.setText("ftp://" + ip + ":2020");
                }
            }
        }
    }

	public boolean isOnline() {
		if (getActivity() != null) {
			ConnectivityManager cm =
				(ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnectedOrConnecting()) {
				return true;
			}
		}
		return false;			
	}
	
}

