package com.koandr.organizer;

import android.content.Context;
import android.content.res.TypedArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Andrew on 9/26/14.
 */
public class NotesCard {

    private String text, date;

    public void setText(String text) {
        this.text = text;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public String getDate() {
        return date;
    }

    /*public String getDateShortly() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd hh:mm", Locale.ENGLISH);
        Calendar calendar = Calendar.getInstance();;
        try {
            calendar.setTime(format.parse(date));
        } catch (ParseException e) {

        }
        String dateShortly = String.valueOf(calendar.get(Calendar.DATE)) + "." +
                String.valueOf(calendar.get(Calendar.MONTH)+1);
        return dateShortly;
    }*/

    public String getDateExpanded(Context th) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd hh:mm", Locale.ENGLISH);
        Calendar calendar = Calendar.getInstance();;
        try {
            calendar.setTime(format.parse(date));
        } catch (ParseException e) {

        }
        TypedArray nameMonth = th.getResources().obtainTypedArray(R.array.months);
        String dateExpanded = nameMonth.getString(calendar.get(Calendar.MONTH))+ " " +
            String.valueOf(calendar.get(Calendar.DATE)) + ", " +
            String.valueOf(calendar.get(Calendar.YEAR)) + ", " +
            String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)) + ":";
        String minute = String.valueOf(calendar.get(Calendar.MINUTE));
        if (minute.length() == 1) {
            minute = "0" + minute;
        }
        dateExpanded += minute;
        return dateExpanded;
    }

}
