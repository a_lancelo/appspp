package com.koandr.organizer;
/**
 * Created by Andrew on 10/16/14.
 */
public class AppScrollTextView extends android.widget.TextView {

    public AppScrollTextView(android.content.Context context, android.util.AttributeSet attrs,
                             int defStyle) {
        super(context, attrs, defStyle);
    }

    public AppScrollTextView(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public AppScrollTextView(android.content.Context context) {
        super(context);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, android.graphics.Rect previouslyFocusedRect) {
        /*if (focused) {
            super.onFocusChanged(focused, direction, previouslyFocusedRect);
        }*/
    }

    @Override
    public void onWindowFocusChanged(boolean focused) {
        /*if (focused) {
            super.onWindowFocusChanged(focused);
        }*/
    }

    @Override
    public boolean isFocused() {
        return true;
    }

}
