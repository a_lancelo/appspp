package com.koandr.organizer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Andrew on 9/26/14.
 */
public class NotesViewerFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.notes);
        View rootView = inflater.inflate(R.layout.notes_viewer, container, false);
        showNoteCard(rootView);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.notes_viewer, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int position = getActivity().getIntent().getIntExtra("note", 0);
        switch (item.getItemId()) {
            case R.id.action_edit:
                Intent intent = new Intent(getActivity(), AppMainActivity.class);
                intent.putExtra("screen", AppConst.NotesEditor);
                intent.putExtra("note", position);
                startActivityForResult(intent, 1);
                break;
            case R.id.action_remove:
                ArrayList<NotesCard> cards = new NotesStorage().getCards(getActivity());
                cards.remove(position);
                new NotesStorage().setCards(getActivity(), cards);
                getActivity().finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ((AppMainActivity) getActivity()).onSelectItem(resultCode, data);
        if (resultCode == 1) {
            showNoteCard(getView());
        }
    }

    private void showNoteCard(View view) {
        int position = getActivity().getIntent().getIntExtra("note", 0);
        NotesCard card = new NotesStorage().getCards(getActivity()).get(position);
        ((TextView) view.findViewById(R.id.date)).setText(card.getDateExpanded(getActivity()));
        ((TextView) view.findViewById(R.id.textView)).setText(card.getText());
    }

}