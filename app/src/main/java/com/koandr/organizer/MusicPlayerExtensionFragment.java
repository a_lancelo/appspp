package com.koandr.organizer;

import java.util.ArrayList;
import java.util.List;

import android.app.*;
import android.content.*;
import android.database.*;
import android.media.AudioManager;
import android.net.Uri;
import android.os.*;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import android.provider.*;
import android.support.v4.app.Fragment;
	
public class MusicPlayerExtensionFragment extends Fragment {
	
		private List<String> data = new ArrayList<String>();
		private List<String> names = new ArrayList<String>();
		SeekBar seekBar;
		TextView duration, current, name;
		Button play;
		int position, progress;
		public static String MY_PREF = "MY_PREF";
		
		@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
			getActivity().setTitle(R.string.music);
	        getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);
	        loadPreferences();
	        View rootView = inflater.inflate(R.layout.music_player_extension, container, false);
	        play = (Button) rootView.findViewById(R.id.play);
			duration= (TextView) rootView.findViewById(R.id.duration);
			current= (TextView) rootView.findViewById(R.id.current);
			name = (TextView)rootView.findViewById(R.id.name);
			seekBar = (SeekBar) rootView.findViewById(R.id.seekBar);
			seekBar.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View view, MotionEvent event) {
						if (data.size() != 0) {
							if (!musicService.isStopwatchRunning()) {
								musicService.start();
							}
							musicService.setProgress(seekBar.getProgress());
						}
						return false;
					}
				});
			rootView.findViewById(R.id.prev).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (data.size() != 0) {
							musicService.previousSing();
						}
					}
				});
			rootView.findViewById(R.id.play).setOnClickListener(new OnClickListener() {
					@SuppressWarnings("deprecation")
					@Override
					public void onClick(View v) {
						if (data.size()!=0) {
							if (musicService.isStopwatchRunning()) {		
								position = musicService.getPosition();
								progress = (int) musicService.getStateDuration();
								musicService.pause();
								savePreferences();
								play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic1));
							} else {
								if (position >= data.size()) position = data.size()-1;
								musicService.setPosition(position);
								musicService.setProgress(progress);
								musicService.start();		
								play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic3));
							}
						}
					}
				});
			rootView.findViewById(R.id.next).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (data.size() != 0) {
							musicService.nextSing();
						}
					}
				});	
			BindAllSongs();
			if (data.size() != 0) {
				if (position >= data.size()) position = data.size()-1;
				name.setText(names.get(position));	
				getActivity().getApplication().startService(new Intent(getActivity(), MusicPlayerService.class));
				bindStopwatchService();		
				mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);			
			}
			return rootView;
		}		
		
		/*OnItemClickListener itemListener = new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
									int position, long id) { 				
				if (musicService!=null) musicService.reset();				
				musicService.setPosition(position);
				musicService.start();	
				play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic3));
			}
		};*/
		
		public void BindAllSongs() {        
			/** Making custom drawable */
			String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
			final String[] projection = new String[] {
				MediaStore.Audio.Media.DISPLAY_NAME,
				MediaStore.Audio.Media.ARTIST,
				MediaStore.Audio.Media.DATA};
			final String sortOrder = MediaStore.Audio.AudioColumns.TITLE
				+ " COLLATE LOCALIZED ASC";

			Cursor cursor = null;
			try {
				Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				cursor = getActivity().getContentResolver().query(uri, projection, selection, null, sortOrder);
				if (cursor != null) {
					cursor.moveToFirst();                       
					while(!cursor.isAfterLast()) { 
						data.add(cursor.getString(2));
						names.add(cursor.getString(0).substring(
						0,cursor.getString(0).length()-4));
						cursor.moveToNext();
					}
				}
			} catch (Exception ex) {

			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}       

		}

		@Override
		public void onResume(){
			super.onResume();
		}

	// Timer to update the elapsedTime display
    private final long mFrequency = 100; // milliseconds
    private final int TICK_WHAT = 2; 
	private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
        	updateElapsedTime();
        	sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

    // Connection to the backgorund StopwatchService
	private MusicPlayerService musicService;
	private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			musicService = ((MusicPlayerService.LocalBinder)service).getService();
			showCorrectButtons();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			musicService = null;
		}
	};
	
	private void bindStopwatchService() {
		getActivity().getApplication().bindService(new Intent(getActivity(), MusicPlayerService.class), 
					serviceConnection , Context.BIND_AUTO_CREATE);
	}
	
	private void unbindStopwatchService() {
		if (musicService != null) {
			getActivity().getApplication().unbindService(serviceConnection );
		}
	}
	
    @Override
	public void onDestroy() {
        super.onDestroy();
        unbindStopwatchService();
    }
    
    @SuppressWarnings("deprecation")
	private void showCorrectButtons() {
    	if (musicService != null) {
    		musicService.setListSong(data);
			//recoverySing();
    		if (musicService != null && musicService.isStopwatchRunning()) {
    			//play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic3));
    		}
    	}
    }
	
	/*private void recoverySing(){
		//loadPreferences();
		if (data.size()<position&data.size()!=0){
			position = 0;
		}	
		
		if (data.size() > position){
			MediaPlayer mp = new MediaPlayer();
			try {
				mp.reset();
				mp.setDataSource(names.get(position));
				mp.prepare();
				mp.start();		
				if (mp.getDuration()>=progress){	
					seekBar.setMax(mp.getDuration());//prefDur);
					//m_stopwatchService.setTime(prefStateDur);		
					seekBar.setProgress(progress);	
					
					current.setText(m_stopwatchService.getFormatTime(progress));
				}			
				//mp.seekTo(MPSB.getProgress());			
				//Current.setText(m_stopwatchService.getFormatTime(prefStateDur));
				duration.setText(m_stopwatchService.getFormatTime(mp.getDuration()));
				name.setText(names2.get(position));		
				mp.pause();		
				// Setup listener so next song starts automatically
				mp.setOnCompletionListener(new OnCompletionListener() {
						public void onCompletion(MediaPlayer arg0) {
							//nextSing();
						}
					});
			} catch (IOException e) {
				//	Log.v(getString(R.string.app_name), e.getMessage());
			}					
		}	
	}*/
    
 /*   private void showPauseLapButtons() {
    	//Log.d(TAG, "showPauseLapButtons");
    	
    	/*m_start.setVisibility(View.GONE);
    	m_reset.setVisibility(View.GONE);
    	m_pause.setVisibility(View.VISIBLE);
    	m_lap.setVisibility(View.VISIBLE);***
    }
    
    private void showStartResetButtons() {
    	//Log.d(TAG, "showStartResetButtons");

    	/*m_start.setVisibility(View.VISIBLE);
    	m_reset.setVisibility(View.VISIBLE);
    	m_pause.setVisibility(View.GONE);
    	m_lap.setVisibility(View.GONE);***
    }
    
  */
    
	public void updateElapsedTime() {
    	if (musicService != null && musicService.isStopwatchRunning()) {
			current.setText(musicService.getFormattedElapsedTime());	
			duration.setText(musicService.getDuration());
			seekBar.setMax(musicService.getIntDuration());
			seekBar.setProgress((int) musicService.getStateDuration());
			name.setText(musicService.getSongName());
		}
    }
	
	public void loadPreferences() {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getActivity().getSharedPreferences(MY_PREF, mode); 
		position = mySharedPreferences.getInt("music_player_position", 0);
		progress = mySharedPreferences.getInt("music_player_progress", 0);
	}

	protected void savePreferences() {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getActivity().getSharedPreferences(MY_PREF, mode); 	
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("music_player_position", position);
		editor.putInt("music_player_progress", progress);
		editor.commit();
	}
	
}

