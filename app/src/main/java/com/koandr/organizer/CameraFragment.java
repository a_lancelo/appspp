package com.koandr.organizer;

import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
//import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.OutputStream;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

public class CameraFragment extends Fragment {

	ImageButton capture, recapture, save;
	CameraPreview preview;
	Camera mCamera;
	FrameLayout mFrame;
	//LinearLayout mButtons;
	Context mContext;
	Uri fileUri;
	Intent intent;
	byte[] pictureData;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.camera_main, container, false);
		mContext = getActivity();//this;

        // кнопки
		//mButtons = (LinearLayout) rootView.findViewById(R.id.buttons);

		capture = (ImageButton) rootView.findViewById(R.id.capture);
		capture.setOnClickListener(
			new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mCamera.takePicture(null, null, null, mPictureCallback);
				}
			}
		);
		
		return rootView;
	}
	
	private Camera openCamera() {
		if (!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
	        return null;

		Camera cam = null;
		if (Camera.getNumberOfCameras() > 0) {
			try {
				cam = Camera.open(0);
			}
			catch (Exception exc) {
				//
			}
		}

		return cam; 
	}

	private PictureCallback mPictureCallback = new PictureCallback() {

	    @Override
	    public void onPictureTaken(byte[] data, Camera camera) {

	    	/*if (intent.getAction().equals (MediaStore.ACTION_IMAGE_CAPTURE)) {
	    		pictureData = data;
	    		showConfirm ();
	    	}
	    	else {*/
	    		Uri pictureFile = generateFile();
	    		try {
					savePhotoInFile (data, pictureFile);
					Toast.makeText(mContext, "Save file: " + pictureFile, Toast.LENGTH_LONG).show();
					getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"+ Environment.getExternalStorageDirectory())));
				} catch (Exception e) {
					Toast.makeText(mContext, "Error: can't save file", Toast.LENGTH_LONG).show();
				}
	    		mCamera.startPreview();
	    	//}
	    }
	};

	private void showConfirm() {
		capture.setVisibility(View.INVISIBLE);
		//mButtons.setVisibility(View.VISIBLE);
    }

	private void hideConfirm() {
		//mButtons.setVisibility(View.INVISIBLE);
		capture.setVisibility(View.VISIBLE);
	}

	private void savePhotoInFile(byte[] data, Uri pictureFile) throws Exception {

		if (pictureFile == null)
			throw new Exception();

       	OutputStream os = getActivity().getContentResolver().openOutputStream(pictureFile);
        os.write(data);
        os.close();
	}

	@Override
    public void onPause() {
		super.onPause();
        if (mCamera != null){
        	mCamera.stopPreview();
            mCamera.release();
            mFrame.removeView(preview);
            mCamera = null;
            preview = null;
        }
    }

	@Override
	public void onResume() {
		super.onResume();
		mCamera = openCamera ();
		if (mCamera == null) {
			Toast.makeText(getActivity(), "Opening camera failed", Toast.LENGTH_LONG).show();
			return;
		}

		preview = new CameraPreview (getActivity(), mCamera);
		mFrame = (FrameLayout) getView().findViewById(R.id.layout);
		mFrame.addView(preview, 0);
    }

	private Uri generateFile() {
		// Проверяем доступность SD карты
		if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
			return null;

		// Проверяем и создаем директорию
		File path = new File (Environment.getExternalStorageDirectory(), "CameraTest");
		if (! path.exists()){
			if (! path.mkdirs()){
				return null;
			}
		}

		// Создаем имя файла
		String timeStamp = String.valueOf(System.currentTimeMillis());
		File newFile = new File(path.getPath() + File.separator + timeStamp + ".jpg");
		return Uri.fromFile(newFile);
	}
	
}
