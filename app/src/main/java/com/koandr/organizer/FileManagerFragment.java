package com.koandr.organizer;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import android.preference.PreferenceManager;
import android.view.*;
import android.support.v4.app.*;
import android.widget.*;
import android.content.*;
import android.os.*;
import android.net.Uri;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.webkit.MimeTypeMap;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;

public class FileManagerFragment extends Fragment {

	private static final String[] Q = new String[] { "Bytes", "Kb", "Mb", "Gb",
			"T", "P", "E" };		
	private ListView list;		
	private ArrayList<String> data = new ArrayList<String>();		
	private ArrayList<String> multiData = new ArrayList<String>();		
	private ArrayList<String> directory = new ArrayList<String>();		
	private String curDir = Environment.getExternalStorageDirectory().getPath();
	private ActionMode actionMode;
	private boolean hidden, multiSelect = false, openFileMode = false;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.file_manager_main, container, false);	
		list = (ListView) rootView.findViewById(R.id.list);	
		rootView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					hidePanel();					
				}
			});
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        hidden = prefs.getBoolean("file_manager_show_hidden_files", false);
        String path = getActivity().getIntent().getStringExtra("path");
        if (path != null) {
            if (path.length() != 0 && new File(path).getParentFile().canRead()) {
                curDir = new File(path).getParent();
            }
            openFileMode = true;
            getActivity().setTitle(R.string.open_file);
        }

		moveView(curDir);//Environment.getExternalStorageDirectory().getPath());
		setHasOptionsMenu(true);
		return rootView;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.file_manager, menu);
	    super.onCreateOptionsMenu(menu,inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {		
		doFileOperation(item.getItemId());
	    return super.onOptionsItemSelected(item);
	}	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode) {
			case R.id.add:
				boolean result;
				switch(data.getIntExtra("type", 0)) {
					case R.id.file:
						result = FileManagerFileUtils.createFile(curDir, data.getStringExtra("name"));
						Toast.makeText(getActivity(), getString(result ? R.string.file_created : R.string.file_created_not), Toast.LENGTH_SHORT).show();											
						break;
					case R.id.folder:
						result = FileManagerFileUtils.createDir(curDir, data.getStringExtra("name"));
						Toast.makeText(getActivity(), getString(result ? R.string.folder_created : R.string.folder_created_not), Toast.LENGTH_SHORT).show();																	
						break;
				}			
				refreshList();
				break;
			case R.id.rename:
				if (data.getStringExtra("name").length() == 0) {					
					return;
				}
				FileManagerFileUtils.renameTarget(data.getStringExtra("path"), data.getStringExtra("name"));
				refreshList();
				break;
			case R.id.delete:
				new BackgroundThread(getActivity(), R.id.delete, curDir).execute(multiData);					
				break;
		}
	}
	
	private OnItemClickListener listListener = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> av, View v, int position, long id) {			
			final String path = data.get(position);
			if (!multiSelect) {
				if (new File(path).isDirectory()) {
					moveView(path);
				} else if (openFileMode) {
                    Intent data = new Intent();
                    data.putExtra("path", path);
                    getActivity().setResult(1, data);
                    getActivity().finish();
                } else {
					String ext = getFileExtension(path);
					if (ext.equals(".zip")) {
						showPanel(R.id.zip, path);						
					} else if (ext.equals(".rar")) {
                        showPanel(R.id.rar, path);
                    } else {
						String mimeType = getMimeType(path);
						Intent myIntent = new Intent();
						myIntent.setAction(android.content.Intent.ACTION_VIEW);
						myIntent.setDataAndType(Uri.fromFile(new File(path)), mimeType);
						try {
							startActivity(myIntent);
						} catch (Exception e) {						
							Intent intent = new Intent();
							intent.setAction(android.content.Intent.ACTION_VIEW);
							intent.setDataAndType(Uri.fromFile(new File(path)), "*/*");
							try {
								startActivity(myIntent);
							} catch (Exception ex) {
							
							}
						}
					}
				}
			} else {
				int last = multiData.size();
				for (int i = multiData.size()-1; i >= 0; i--) {
					if (multiData.get(i).equals(data.get(position))) {
						multiData.remove(i);		
						v.setBackgroundColor(-1);		
					}
				}				
				if (last == multiData.size()) {
					multiData.add(data.get(position));		
					v.setBackgroundColor(0x1faee900);
				}	
				switch(multiData.size()) {				
					case 0:
						actionMode.finish();
						break;
					case 1:
						actionMode.getMenu().findItem(R.id.rename).setVisible(true);
						if (!new File(multiData.get(0)).isDirectory()) {
							actionMode.getMenu().findItem(R.id.open_as).setVisible(true);
							actionMode.getMenu().findItem(R.id.share).setVisible(true);
						} else {
							actionMode.getMenu().findItem(R.id.open_as).setVisible(false);
							actionMode.getMenu().findItem(R.id.share).setVisible(false);
						}					
						break;
					case 2:
						actionMode.getMenu().findItem(R.id.open_as).setVisible(false);
						actionMode.getMenu().findItem(R.id.rename).setVisible(false);
						actionMode.getMenu().findItem(R.id.share).setVisible(true);
						break;
				}								
			}						
		}		
	};

	private OnItemLongClickListener listLongListener = new OnItemLongClickListener() {

		public boolean onItemLongClick(AdapterView<?> av, View v,
									   int position, long id) {
			if (!multiSelect & !curDir.equals("/mnt")) {
				hidePanel();
				multiData.clear();
				actionMode = ((ActionBarActivity) getActivity()).startSupportActionMode(callback);
				multiData.add(data.get(position));	
				v.setBackgroundColor(0x1faee900);								
				actionMode.getMenu().findItem(R.id.zip).setVisible(false);
				actionMode.getMenu().findItem(R.id.rar).setVisible(false);									
			}							   										   
			return true;
		}

	};

	private ActionMode.Callback callback = new ActionMode.Callback() {
		
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			multiSelect = true;
			mode.getMenuInflater().inflate(R.menu.file_manager_action, menu);
			return true;
		}

		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			doFileOperation(item.getItemId());				
			mode.finish();		
			return false;
		}

		public void onDestroyActionMode(ActionMode mode) {
			multiSelect = false;
			for (int i = 0; i < list.getChildCount(); i++){
				list.getChildAt(i).setBackgroundColor(-1);				
			}
		}

		public void onItemCheckedStateChanged(ActionMode mode,
											  int position, long id, boolean checked) {
		}
		
	};

	private final static Comparator<? super String> name = new Comparator<String>() {
		public int compare(String arg0, String arg1) {
			return arg0.toLowerCase().compareTo(arg1.toLowerCase());
		}
	};
	
	public boolean onBackPressed() {
		if (!new File(curDir).getParent().equals("/")
			& !new File(curDir).getParent().equals("/mnt")) {
			moveView(new File(curDir).getParent());
			return true;
		}
		return false;
	}
	
	private void refreshList() {
		int index = list.getFirstVisiblePosition();				
		moveView(curDir);
		list.setSelectionFromTop(index, 0);		
	}
	
	private void showPanel(int id) {
		showPanel(id, null);
	}
	
	private void showPanel(final int id, final Object data) {
		list.setPadding(0, 0, 0, 50);
		getView().findViewById(R.id.pasteLayout).setVisibility(View.VISIBLE);
		Button button = (Button) getView().findViewById(R.id.paste);
		switch(id) {
			case R.id.copy:
			case R.id.cut:
				button.setText(R.string.paste);
				break;
			case R.id.zip:
			case R.id.rar:
				button.setText(R.string.extract);
				break;					
		}
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				doFileOperationSecondary(id, data);			
				hidePanel();
			}
		});			
	}
	
	private void hidePanel() {
		list.setPadding(0, 0, 0, 0);
		getView().findViewById(R.id.pasteLayout).setVisibility(View.GONE);		
	}
	
	private void doFileOperationSecondary(int id, Object data) {
		switch(id) {
			case R.id.copy:
				new BackgroundThread(getActivity(), id, curDir).execute(multiData);	
				break;
			case R.id.cut:
				String sub = multiData.get(0).substring(5, multiData.get(0).length())+"/";
				sub = sub.substring(0, sub.indexOf("/"));							
				String sub2 = curDir.substring(5, curDir.length())+"/";
				sub2 = sub2.substring(0, sub2.indexOf("/"));		
				if (sub.equals(sub2)) {
					boolean result = false;
					for (int i = 0; i < multiData.size(); i++) {
						result = FileManagerFileUtils.fastCutToDirectory(multiData.get(i), curDir);		
					}
					Toast.makeText(getActivity(), getString(result ? R.string.cutting_success : R.string.cutting_fail), Toast.LENGTH_SHORT).show();										
				} else {															
					new BackgroundThread(getActivity(), R.id.cut, curDir).execute(multiData);					
				}
				refreshList();	
				break;
			case R.id.zip:
				new BackgroundThread(getActivity(), R.id.zip, curDir).execute(new File((String) data));		
				break;
			case R.id.rar:
                new BackgroundThread(getActivity(), R.id.rar, curDir).execute(new File((String) data));
                break;
		}
	}
	
	private void doFileOperation(int id) {		
		switch(id) {
			case R.id.add:
				DialogFragment adderFragment = new FileManagerAdderDialog();				
				adderFragment.setTargetFragment(this, R.id.add);
				adderFragment.show(getFragmentManager(), "dialog");		
				break;
			case R.id.open_as:
				Intent myIntent = new Intent();
				myIntent.setAction(android.content.Intent.ACTION_VIEW);
				myIntent.setDataAndType(Uri.fromFile(new File(multiData.get(0))), "*/*");
				try {
					startActivity(myIntent);
				} catch (Exception e) {
				
				}
				break;
			case R.id.rename:					
				DialogFragment renamerFragment = new FileManagerRenamerDialog();				
				Bundle arguments = new Bundle();
				arguments.putString("path", multiData.get(0));
				renamerFragment.setArguments(arguments);
				renamerFragment.setTargetFragment(this, R.id.rename);
				renamerFragment.show(getFragmentManager(), "dialog");							
				break;			
			case R.id.select_all:
				actionMode = ((ActionBarActivity) getActivity()).startSupportActionMode(callback);
				actionMode.getMenu().findItem(R.id.open_as).setVisible(false);
				actionMode.getMenu().findItem(R.id.rename).setVisible(false);
				multiData.clear();
				for (int i = 0; i < data.size(); i++) {
					multiData.add(data.get(i));
				}					
				for (int i = 0; i < list.getChildCount(); i++) {
					list.getChildAt(i).setBackgroundColor(0x1faee900);				
				}
				break;	
			case R.id.copy:
			case R.id.cut:
				showPanel(id);
				break;				
			case R.id.share:
				ArrayList<Uri> uris = new ArrayList<Uri>();
				int length = multiData.size();
				Intent send_intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
				send_intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
				send_intent.setType("image/jpeg");
				for (int i = 0; i < length; i++) {
					File file = new File(multiData.get(i))
						.getAbsoluteFile();
					uris.add(Uri.fromFile(file));
				}
				send_intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
				startActivity(Intent.createChooser(send_intent, "Send via.."));
				break;
			case R.id.delete:
				DialogFragment removerFragment = new FileManagerRemoverDialog();				
				removerFragment.setTargetFragment(this, R.id.delete);
				removerFragment.show(getFragmentManager(), "dialog");	
				break;				
			case R.id.go_home:
				moveView("/mnt");
				break;
		}
				
	}
	
	public void moveView(String path) {
        getActivity().setTitle(path);
		File file = new File(path);
		if (file.canRead()) {
			data.clear();
			directory.clear();
			curDir = path;
			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				File fileItem = files[i];
				if (fileItem.getName().toString().startsWith(".")) {
					if (hidden) {
						data.add(fileItem.getPath());
						directory.add(fileItem.getName());
					}
				} else {
					data.add(fileItem.getPath());
					directory.add(fileItem.getName());
				}
			}		
			ArrayList<String> dFolders = new ArrayList<String>();
			ArrayList<String> dFiles = new ArrayList<String>();				
			String [] paths = new String[data.size()];
			data.toArray(paths);
			Arrays.sort(paths, name);
			data = new ArrayList<String>(Arrays.asList(paths));						
			paths = new String[directory.size()];
			directory.toArray(paths);
			Arrays.sort(paths, name);
			directory = new ArrayList<String>(Arrays.asList(paths));
			for (int i = 0; i < directory.size(); i++)
				if (new File(data.get(i)).isDirectory()) dFolders.add(directory.get(i));
				else dFiles.add(directory.get(i));
			directory = new ArrayList<String>();
			directory.addAll(dFolders);
			directory.addAll(dFiles);			
			dFolders.clear();
			dFiles.clear();					
			for (int i = 0; i < data.size(); i++)
				if (new File(data.get(i)).isDirectory()) dFolders.add(data.get(i));
					else dFiles.add(data.get(i));
			data = new ArrayList<String>();
			data.addAll(dFolders);
			data.addAll(dFiles);
			list.setOnItemClickListener(listListener);
			list.setOnItemLongClickListener(listLongListener);
			list.setAdapter(new FileManagerListAdapter(getActivity(), data));
		}
	}
	
	public String getAsString(long bytes) {
		for (int i = 6; i >= 0; i--) {
			double step = Math.pow(1024, i);
			if (bytes > step)
				return String.format("%3.2f %s", bytes / step, Q[i]);
		}
		return "0.00 "+Q[0];
	}

	public long getSize(File file) {
		long size = 0;
		int len = 0;
		if (file.isFile()) {
			size = file.length();
		} else if (file.isDirectory()) {
			File[] list = file.listFiles();
			if (list != null) {
				len = list.length;
			}
			for (int j = 0; j < len; j++) {
				if (list[j].isFile()) {
					size = size + list[j].length();
				} else if (list[j].isDirectory()) {
					size = size + getSize(list[j]);
				}
			}
		}
		return size;
	}
	
	private String getMimeType(String filename) {
		String extension = getFileExtension(filename).toLowerCase();
		if (extension.length() > 0) {
			String webkitMimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.substring(1));
			return webkitMimeType == null ? "*/*" : webkitMimeType;
		}
		return "*/*";
	}	
	
	private String getFileExtension(String filepath) {
		File file = new File(filepath);
		String filename = file.getName().toString();
		String ext = null;
		try {
			ext = filename.substring(filename.lastIndexOf("."),
									 filename.length()).toLowerCase();
		} catch (IndexOutOfBoundsException e) {
			ext = "";
		}
		return ext;
	}
	
	private class FileManagerListAdapter extends BaseAdapter {

		private FileManagerImageLoader imageLoader;
		private FileManagerDrawableLoader drawableLoader;
		private Context context;
		private LayoutInflater lInflater;
		private ArrayList<String> items;
		
		public FileManagerListAdapter(Context context, ArrayList<String> items) {
			this.context = context;
			this.items = items;
			lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			imageLoader = new FileManagerImageLoader(getActivity());
			drawableLoader = new FileManagerDrawableLoader(getActivity());
		}
		
		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			return items.get(position);
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = lInflater.inflate(R.layout.file_manager_main_list_row, parent, false);
			TextView name = (TextView) view.findViewById(R.id.name);
			name.setText(new File((String) getItem(position)).getName());
			ImageView image = (ImageView) view.findViewById(R.id.image);
			TextView size = (TextView) view.findViewById(R.id.size);									
			final String filepath = (String) getItem(position);
			File f = new File(filepath);
			if (f.isFile()) {
				size.setText(getAsString(new File(items.get(position)).length()));
			}			
			String ext = getFileExtension(filepath);
			if (f.isFile() && ext.equals(".apk")) {
				drawableLoader.displayImage(filepath, getActivity(), image);
			} else if (f.isFile() && ext.equals(".jpg") || ext.equals(".png")
					 || ext.equals(".gif") || ext.equals(".jpeg")
					 || ext.equals(".tiff") || ext.equals(".bmp")) {
				imageLoader.displayImage(filepath, getActivity(), image);				
			} else {
				image.setImageDrawable(getIcon(filepath));
			}
			if (multiSelect) {
				for (int i = 0; i < multiData.size(); i++) {
					if (multiData.get(i).equals((String) getItem(position))) {
						view.setBackgroundColor(0x1faee900);
						break;
					}
				}
			}
			return view;
		}
		
		private Drawable getIcon(String filepath) {
			File file = new File(filepath);		
			String ext = getFileExtension(filepath);
			if (file.isDirectory()) {
				return getResources().getDrawable(R.drawable.folder1png);
			} else if (file.isFile()) {
				if (ext.equals(".zip")) {
					return getResources().getDrawable(R.drawable.file_manager_zip);
				} else if (ext.equals(".rar")) {
					return getResources().getDrawable(R.drawable.file_manager_rar);
				} else if (ext.equals(".pdf")) {
					return getResources().getDrawable(R.drawable.file_manager_pdf);
				} else if (ext.equals(".jpg") || ext.equals(".png") || ext.equals(".gif")
						|| ext.equals(".jpeg") || ext.equals(".tiff") || ext.equals(".bmp")) {
					return getResources().getDrawable(R.drawable.file_manager_image);
				} else if (ext.equals(".mp3") || ext.equals(".wav") || ext.equals(".m4a")) {
					return getResources().getDrawable(R.drawable.file_manager_audio);
				} else if (ext.equals(".apk")) {
					return getResources().getDrawable(R.drawable.file_manager_apk);
				} else if (ext.equals(".doc") || ext.equals(".docx") || ext.equals(".rtf")
						|| ext.equals(".djvu") || ext.equals(".epub") || ext.equals(".mobi")
						|| ext.equals(".txt") || ext.equals(".html") || ext.equals(".htm")  
						|| ext.equals(".xml") || ext.equals(".java") || ext.equals(".odt")
						|| ext.equals(".fb2")) {
					return getResources().getDrawable(R.drawable.file_manager_text);
				} else {
					return getResources().getDrawable(R.drawable.file_manager_unknown);
				}
			} else {
				return getResources().getDrawable(R.drawable.file_manager_unknown);
			}
		}
		
		private String getFileExtension(String filepath) {
			File file = new File(filepath);
			String filename = file.getName().toString();
			String ext = null;
			try {
				ext = filename.substring(filename.lastIndexOf("."),
										 filename.length()).toLowerCase();
			} catch (IndexOutOfBoundsException e) {
				ext = "";
			}
			return ext;
		}
		
	}
	
	public class BackgroundThread extends AsyncTask<Object, Void, Boolean> {
		
		int type;
		boolean success;		
		String newDir;
		ProgressDialog dialog;
		Context context;
		
		public BackgroundThread(Context context, int type, String newDir) {
			this.context = context;
			this.type = type;
			this.newDir = newDir;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(context);
			dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			switch(type) {
				case R.id.copy:
					dialog.setTitle(R.string.copying_files);
					break;
				case R.id.cut:
					dialog.setTitle(R.string.cutting_files);		
					break;
				case R.id.delete:
					dialog.setTitle(R.string.deleting_files);			
					break;
				case R.id.zip:
					dialog.setTitle(R.string.unzipping_files);		
					break;
                case R.id.rar:
                    dialog.setTitle(R.string.unzipping_files);
                    break;
			}
			dialog.show();
		}
		
		@Override
		protected Boolean doInBackground(Object... params) {
			switch (type) {
				case R.id.copy:
				case R.id.cut:
				case R.id.delete:
					return multiOperation(type, (ArrayList<String>) params[0]);					
				case R.id.zip:
					try {
						ZipFile zipfile = new ZipFile((File) params[0]);
						int fileCount = zipfile.size();
						dialog.setMax(fileCount);
						for (Enumeration e = zipfile.entries(); e.hasMoreElements();) {
							ZipEntry entry = (ZipEntry) e.nextElement();
							FileManagerFileUtils.unzipEntry(zipfile, entry, newDir);
							dialog.setProgress((dialog.getProgress()+1 * 100)/ fileCount);
						}						
					} catch (Exception e) {
						return false;
					}				
					break;
                case R.id.rar:
                    FileManagerExtractRarArchive.extractArchive(((File) params[0]).toString(), newDir);
                    break;
			}
			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
            if (getActivity() == null) {
                return;
            }
			switch(type) {
				case R.id.copy:
					Toast.makeText(context, getString(result ? R.string.copying_success : R.string.copying_fail), Toast.LENGTH_SHORT).show();
					refreshList();
					dialog.dismiss();
					break;
				case R.id.cut:
					Toast.makeText(context, getString(result ? R.string.copying_success : R.string.copying_fail), Toast.LENGTH_SHORT).show();					
					dialog.dismiss();
					break;
				case R.id.delete:
					Toast.makeText(context, getString(result ? R.string.deleting_success : R.string.deleting_fail), Toast.LENGTH_SHORT).show();
					refreshList();
					dialog.dismiss();
					break;
				case R.id.zip:
					Toast.makeText(context, getString(result ? R.string.unzipping_success : R.string.unzipping_fail), Toast.LENGTH_SHORT).show();
					refreshList();
					dialog.dismiss();
					break;
                case R.id.rar:
                    Toast.makeText(context, getString(result ? R.string.unzipping_success : R.string.unzipping_fail), Toast.LENGTH_SHORT).show();
                    refreshList();
                    dialog.dismiss();
                    break;
			}
		}
		
		private boolean multiOperation(int id, ArrayList<String> data) {
			try {
				dialog.setProgress(data.size());
				for (int i = 0; i < data.size(); i++) {
					switch(id) {
						case R.id.copy:
						case R.id.cut:
							if (!FileManagerFileUtils.copyToDirectory(data.get(i), newDir)) {
								return false;
							}				
						break;
						case R.id.delete:
							FileManagerFileUtils.deleteTarget(data.get(i));
							break;
					}
					dialog.setProgress((dialog.getProgress()+1 * 100)/data.size());
				}
			} catch (Exception e) {
				return false;
			}										
			if (id == R.id.cut) {							
				new BackgroundThread(getActivity(), R.id.delete, curDir).execute(data);										
			}
			return true;
		}
		
	}
			
}
