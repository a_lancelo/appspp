package com.koandr.organizer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;

public class NewsRSSBookmarksEditorDialog extends DialogFragment {
	
	private EditText editText;
	
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		editText = new EditText(getActivity());
		editText.setText(getArguments().getString("url"));
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
	  		.setTitle(getArguments().getString("title"))
	  		.setView(editText)
	  		.setNegativeButton(R.string.delete, listener)
	  		.setPositiveButton(R.string.ok, listener);
		return adb.create();
	}
  
	DialogInterface.OnClickListener listener = new OnClickListener() {
	
		@Override
		public void onClick(DialogInterface dialog, int which) {
			Intent data = new Intent();
			data.putExtra("position", getArguments().getInt("position"));
			switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					data.putExtra("url", editText.getText().toString());
					getTargetFragment().onActivityResult(getTargetRequestCode(), 1, data);
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					getTargetFragment().onActivityResult(getTargetRequestCode(), 2, data);
					break;
			}
			dismiss();
		}
	  
	};
	  
}
