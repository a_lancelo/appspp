package com.koandr.organizer;

import android.support.v4.app.*;
import android.annotation.SuppressLint;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.*;
import android.webkit.*;

public class NewsBrowserFragment extends Fragment {
	
    WebView mWebView;
	ProgressBar bar;
	
	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		getActivity().setTitle(R.string.news_rss);
		View rootView = inflater.inflate(R.layout.news_browser_main, container, false);
		mWebView = (WebView) rootView.findViewById(R.id.browser);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setPluginState(PluginState.ON);
		mWebView.getSettings().getDefaultZoom();
		//mWebView.getSettings().setDisplayZoomControls(true);
		mWebView.getSettings().setTextZoom(5);
		mWebView.getSettings().setSupportZoom(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.loadUrl(getActivity().getIntent().getStringExtra("url"));//getArguments().getString("url"));
		bar = (ProgressBar)rootView.findViewById(R.id.loadingBar);
		mWebView.setWebChromeClient(new WebChromeClient(){
				public void onProgressChanged(WebView view, int progress){
					// If loading isn't complete, make sure the loading bar is visible
					if(progress < 100 && bar.getVisibility() == ProgressBar.GONE){
						// Set the loading bar as visible
						bar.setVisibility(ProgressBar.VISIBLE);
					}
					// Update the loading bar
					bar.setProgress(progress);
					// If loading is done
					if(progress == 100){
						// Remove the loading bar
						bar.setVisibility(ProgressBar.GONE);
						// Display the WebView
						//browser.setVisibility(View.VISIBLE);
						// Dismiss the dialog
						//loadingDialog.dismiss();
					}
				}
			});
		
		mWebView.setWebViewClient(new HelloWebViewClient());
		return rootView;
	}
	
	/*@Override
	public void onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
			mWebView.goBack();
			//return true;
		}
		//return super.onKeyDown(keyCode, event);
	}*/

	private class HelloWebViewClient extends WebViewClient {
		
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
		
	}
	
}
