package com.koandr.organizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class TextEditorDialogEncoding extends DialogFragment implements OnClickListener {

	  final String LOG_TAG = "myLogs";
	  public String cod="Windows-1251";

	  public static TextEditorDialogEncoding newInstance(int num){

		  TextEditorDialogEncoding dialogFragment = new TextEditorDialogEncoding();
		    Bundle bundle = new Bundle();
		    bundle.putInt("num", num);
		    dialogFragment.setArguments(bundle);

		    return dialogFragment;
	  }

	  
	  public Dialog onCreateDialog(Bundle savedInstanceState) {
		final String[] Encoding = {"Windows-1251","UTF-8"};
		  		  
		Bundle bundle = getArguments();
		cod = bundle.getString("cod");
		int number = 1;
		
		if (cod.equals(Encoding[0])){
			number = 0;
		}
		
	    AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
	        .setTitle(R.string.encoding)
	        //.setMessage(R.string.about_app)
	        .setSingleChoiceItems(Encoding, number, 
	        	new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int item) {
					cod=Encoding[item];
				}
			})
	        .setNeutralButton(R.string.back, this);
	    return adb.create();
	  }

	 /* public void onClick(DialogInterface dialog, int which) {
	    int i = 0;
	    switch (which) {
	    case Dialog.BUTTON_POSITIVE:
	      i = R.string.yes;
	      break;
	    case Dialog.BUTTON_NEGATIVE:
	      i = R.string.no;
	      break;
	    case Dialog.BUTTON_NEUTRAL:
	      i = R.string.maybe;
	      break;
	    }
	    if (i > 0)
	      Log.d(LOG_TAG, "Dialog 2: " + getResources().getString(i));
	  }*/

	  public void onDismiss(DialogInterface dialog) {
	    super.onDismiss(dialog);
	    //Log.d(LOG_TAG, "Dialog 2: onDismiss");
	  }

	  public void onCancel(DialogInterface dialog) {
	    super.onCancel(dialog);
	    //Log.d(LOG_TAG, "Dialog 2: onCancel");
	    Intent i = new Intent();
	    i.putExtra("cod", cod);
	    int INT_CODE = 1;
	    getTargetFragment().onActivityResult(getTargetRequestCode(), INT_CODE, i);
	    dismiss();
	  }

	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		// TODO Auto-generated method stub
		//int i = 0;
	    switch (arg1) {
	    case Dialog.BUTTON_POSITIVE:
	      //i = R.string.yes;
	      getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
	      break;
	    case Dialog.BUTTON_NEGATIVE:
	    	getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, getActivity().getIntent());
	     // i = R.string.no;
	      break;
	    case Dialog.BUTTON_NEUTRAL:
	     // i = R.string.maybe;
	    	Intent i = new Intent();
		    i.putExtra("cod", cod);
		    int INT_CODE = 1;
		    getTargetFragment().onActivityResult(getTargetRequestCode(), INT_CODE, i);
		    dismiss();
	      break;
	    }
	    //if (i > 0)
	      //Log.d(LOG_TAG, "Dialog 2: " + getResources().getString(i));
	}

}
