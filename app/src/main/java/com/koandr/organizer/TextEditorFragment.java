package com.koandr.organizer;

import java.io.*;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.*;
import android.widget.EditText;


public class TextEditorFragment extends Fragment {

	private boolean rsave, rsave2 = false;
	private String te_nf, cod = "Windows-1251";
	private int t = 0;
	private EditText text;
	private String recentPath;
    private boolean openRecentFile;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	getActivity().setTitle(R.string.text_editor);
        View rootView = inflater.inflate(R.layout.texteditor_main, container, false);
        /*String[] data = new String[] {getString(R.string.what_do), getString(R.string.newf), 
        		getString(R.string.openf), getString(R.string.savef), 
        		getString(R.string.encod) };
        android.support.v7.app.ActionBar ab;
        ab = ((ActionBarActivity) getActivity()).getSupportActionBar();
        getActivity().setTitle("");
        ab.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_LIST);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
        		android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ab.setListNavigationCallbacks(adapter, this);*/
        text = (EditText) rootView.findViewById(R.id.editText);	
        rsave = true;
	    loadPreferences();
	    setHasOptionsMenu(true);
        return rootView;
    }
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.text_editor_main, menu);
	    super.onCreateOptionsMenu(menu,inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_new_event:
				text.setText("");
				rsave = true;
				rsave2 = false;	
				break;
			case R.id.action_view_as_list:
                Intent intent = new Intent(getActivity(), AppMainActivity.class);
                intent.putExtra("screen", AppConst.FileManager);
                String nsend = te_nf;
                if (!rsave2) {
                    nsend = "";
                }
                intent.putExtra("path", nsend);
                startActivityForResult(intent, 5);

				/*Bundle b = new Bundle();
				String nsend = te_nf;
				if (!rsave2) {
					nsend = "";
				}
				b.putString("dssID", nsend); 
				DialogFragment dialogFragF = TextEditorDialogFileBrowser.newInstance(123);
	            dialogFragF.setTargetFragment(TextEditorFragment.this, 5);
	            dialogFragF.setArguments(b);
	            dialogFragF.show(getFragmentManager().beginTransaction(), "dialog");*/
				break;
			case R.id.action_save:
				if (rsave) {
					if (!rsave2) {
						t++;
						savePreferences();
						rsave2 = true;
					}
					loadPreferences();
					te_nf = "/sdcard/Apps++/"+"Text_"+Integer.toString(t)+".txt";
					String newFolder = "/Apps++";
					try {
						String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
						File myNewFolder = new File(extStorageDirectory + newFolder);
						myNewFolder.mkdir();
					} catch (Exception e) {
						
					}
		        }
				saveFile(te_nf);
				break;	
		}
	    return super.onOptionsItemSelected(item);
	}
	
	/*@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		switch (itemPosition) {
		case 1:
		    //setTitle(getString(R.string.newf));
		    text.setText("");
			rsave = true;
			rsave2 = false;		
			break;
		case 2:
			Bundle b = new Bundle();
			String nsend=te_nf;
			if (!rsave2) {
				nsend="";
			}
			b.putString("dssID", nsend); 
			DialogFragment dialogFragF = TextEditorDialogFileBrowser.newInstance(123);
            dialogFragF.setTargetFragment(TextEditorFragment.this, 5);
            dialogFragF.setArguments(b);
            dialogFragF.show(getFragmentManager().beginTransaction(), "dialog");
            break;
		case 3:
			if (rsave) {
				if (!rsave2) {
					t++;
					savePreferences();
					rsave2=true;
				}
				loadPreferences();
				te_nf="/sdcard/Apps++/"+"Text_"+Integer.toString(t)+".txt";
				String newFolder = "/Apps++";
				try {
					String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
					File myNewFolder = new File(extStorageDirectory + newFolder);
					myNewFolder.mkdir();
				} catch (Exception e) {
					
				}
	        }
			saveFile(te_nf);
			break;
		case 4:
			Bundle bundle = new Bundle();
			bundle.putString("cod", cod); 
			DialogFragment dialogFrag = TextEditorDialogEncoding.newInstance(123);
            dialogFrag.setTargetFragment(TextEditorFragment.this, 1);
            dialogFrag.setArguments(bundle);
            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");
            break;
		}
		
		android.support.v7.app.ActionBar ab = ((ActionBarActivity) getActivity()).getSupportActionBar();
        ab.setSelectedNavigationItem(0);
		return true;
	}*/
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//super.onActivityResult(requestCode, resultCode, data);
        ((AppMainActivity) getActivity()).onSelectItem(resultCode, data);
		if (requestCode == 1) {
			cod = data.getStringExtra("cod");
			openFile(te_nf);
		}
		if (requestCode == 5 & resultCode == 1) {
			te_nf = data.getStringExtra("path");// ("com.koandr.organizer.THIEF");
			rsave = false;
			rsave2 = true;
            if (openRecentFile) {
                recentPath = te_nf;
                savePreferences();
            }
			openFile(te_nf);
		}
	}
	
	private void openFile(String path) {
		try {
			FileInputStream fstream1 = new FileInputStream(path);
			DataInputStream in = new DataInputStream(fstream1);
			InputStreamReader isr = new InputStreamReader(in, cod);
			BufferedReader reader = new BufferedReader(isr);
			String str;
			StringBuffer buffer = new StringBuffer();
			while ((str = reader.readLine()) != null) {
				buffer.append(str + "\n");
			}
			text.setText(buffer.toString());
		} catch (Throwable t) {
			
		}
	}

	private void saveFile(String path) {
		try {
			FileOutputStream fos= new FileOutputStream (path);
			DataOutputStream in = new DataOutputStream(fos);
			OutputStreamWriter osw = new OutputStreamWriter(in,cod);
			BufferedWriter out = new BufferedWriter(osw);
			out.write(text.getText().toString());
			out.close();
		} catch (Exception e) {
			
		}
	}
	
	private void loadPreferences() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		cod = prefs.getString("text_editor_encoding", ""); //Windows-1251");
        openRecentFile = prefs.getBoolean("text_editor_open_recent_file", false);
        if (!prefs.getBoolean("t9", false)) {
            text.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            text.setSingleLine(false);
        }
        prefs = getActivity().getSharedPreferences(AppConst.PreferencesFile, Activity.MODE_PRIVATE);
        if (openRecentFile) {
            recentPath = prefs.getString("TextEditorRecentPath", "");
            te_nf = recentPath;
            if (new File(te_nf).exists()&!new File(te_nf).isDirectory()) {
                rsave = false;
                rsave2 = true;
                openFile(te_nf);
            }
        }
		t = prefs.getInt("TextEditorCountFile", 0);
	}

	private void savePreferences() {	
		Editor editor = getActivity().getSharedPreferences(AppConst.PreferencesFile, Activity.MODE_PRIVATE).edit();
		editor.putInt("TextEditorCountFile", t);
        if (openRecentFile) {
            editor.putString("TextEditorRecentPath", recentPath);
        }
		editor.commit();
	}	
	
}
