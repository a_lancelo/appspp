package com.koandr.organizer;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Andrew on 9/26/14.
 */
public class NotesMainFragment extends Fragment {

    ArrayList<String> notes = new ArrayList<String>();
    ArrayList<String> titles = new ArrayList<String>();
    ArrayList<Integer> targetWas = new ArrayList<Integer>();

    LinearLayout linLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.notes);
        View rootView = inflater.inflate(R.layout.notes_main, container, false);
        PFopenFile("MyNotes");
        LayoutInflater ltInflater=getLayoutInflater(savedInstanceState);
        //LinearLayout
        linLayout = (LinearLayout) rootView.findViewById(R.id.noteslinLayout);
        //linLayout.setBackgroundColor(-1);
        //linLayout.removeAllViews();
        for (int i = 0; i <  notes.size(); i++) {
            View item = ltInflater.inflate(R.layout.notes_main_item, linLayout, false);
            TextView tvName = (TextView) item.findViewById(R.id.notes_main_item_textview);
            item.setId(i);
            item.setOnClickListener(ListOclBtn);
            //if (bV.get(i).length()>10)
            //tvName.setText(bV.get(i).substring(0,10));
            //else
            tvName.setText(titles.get(i));
            if (targetWas.get(i)==1)
                tvName.setPaintFlags(tvName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvName.setTypeface(null, Typeface.NORMAL);
            item.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
            //item.setBackgroundColor(-1179716);
            linLayout.addView(item);
        }

        setHasOptionsMenu(true);
        return rootView;
    }

    /*public void onBackPressed()
    {
        //Handle any cleanup you don't always want done in the normal lifecycle
        if (fragment!=null)
            ((NotesFragmentEdit)fragment).onBackPressed();
    }*/
    Fragment fragment;
    public View.OnClickListener ListOclBtn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //TNote=bV.get(v.getId());
            //Intent intent = new Intent(NotesActivity.this,NotesEditActivity.class);
            //startActivityForResult(intent, v.getId());
            //Fragment
			/*linLayout.removeAllViews();
			notes.clear();
			titles.clear();
			targetWas.clear();*/
			/*fragment = new NotesEditFragment();
			//fragment.se

			Bundle args = new Bundle();
			args.putInt("note", v.getId());
			fragment.setArguments(args);

			FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
			*/

            Intent intent = new Intent(getActivity(), AppMainActivity.class);
            intent.putExtra("screen", AppConst.NotesEditor);
            intent.putExtra("note", v.getId());
            startActivityForResult(intent, 1);

			/*Intent i = new Intent(getActivity(), NotesEditActivity.class);
			i.putExtra("note", v.getId());
	        //startActivity(i);
			startActivityForResult(i, 1);*/
        }
    };

	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    // TODO Add your menu entries here

	    super.onCreateOptionsMenu(menu, inflater);
	    //getActivity().getMenuInflater()
	    inflater.inflate(R.menu.paint_menu, menu);
	}*/


	/*@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, 101, Menu.NONE, getString(R.string.abn));
		return super.onCreateOptionsMenu(menu);
	}*/

	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    //MenuInflater
	    inflater = //getActivity().
	    		getSupportMenuInflater();
	    getMenuInflater();
	    inflater.inflate(R.menu.paint_menu, menu);
	    //return true;
	}*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.notes_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Log.d("MENU", "Cliced MenuItem is " + item.getTitle());
        if (item.getItemId() == R.id.action_item_1){
			/*DialogFragment dialogFrag = NotebookDialogAdd.newInstance(123);
            dialogFrag.setTargetFragment(NotesFragment.this, 1);

            //(this, DIALOG_FRAGMENT);
            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");*/
            //((ActionBarActivity)getActivity()).replaceFragme  (Object... params);
			/*linLayout.removeAllViews();
			notes.clear();
			titles.clear();
			targetWas.clear();
			Fragment  fragment = new NotesEditFragment();
			Bundle args = new Bundle();
			args.putInt("note", -1);
			fragment.setArguments(args);
			FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();*/

            Intent intent = new Intent(getActivity(), AppMainActivity.class);
            intent.putExtra("screen", AppConst.NotesEditor);
            intent.putExtra("note", -1);
            startActivityForResult(intent, 1);
        }
        return super.onOptionsItemSelected(item);
    }

    String Title;
    private void PFopenFile(String fileName) {
        try {
            InputStream inputstream = getActivity().openFileInput(fileName+".txt");
            if (inputstream != null) {
                InputStreamReader isr = new InputStreamReader(inputstream);
                BufferedReader reader = new BufferedReader(isr);
                String str;
                String FSt="";
                StringBuffer buffer = new StringBuffer();
                int i = -1;
                boolean nowWillTitle=true;
                boolean nowWillName=true;
                String name; // name of Notebook

                while ((str = reader.readLine()) != null) {
                    buffer.append(str + "\n");
                    if (str.equals("com.koandr.organizer")==false){
                        if (nowWillName) {
                            name=str;
                            nowWillName=false;
                        }	else {
                            if (nowWillTitle){
                                //Title=str;
                                targetWas.add(Integer.parseInt(str.substring(0,1)));
                                titles.add(str.substring(1));
                                nowWillTitle=false;
                            } else{
                                FSt=FSt+str+"\n";
                                notes.set(i,FSt);
                            }
                        }

                    }else{
                        notes.add("");
                        i++;
                        FSt="";
                        nowWillTitle=true;
                    }
                }
                inputstream.close();
            }
        } catch (Throwable t) {}
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        //super.onActivityResult(requestCode, resultCode, data);
        ((AppMainActivity) getActivity()).onSelectItem(resultCode, data);

        if (resultCode == 1) {


            //Toast.makeText(getActivity(), "trueNotes", 2).show();

            linLayout.removeAllViews();
            notes.clear();
            titles.clear();
            targetWas.clear();

            PFopenFile("MyNotes");
            LayoutInflater ltInflater = getActivity().getLayoutInflater();
            for (int i = 0; i <  notes.size(); i++) {
                View item = ltInflater.inflate(R.layout.notes_main_item, linLayout, false);
                TextView tvName = (TextView) item.findViewById(R.id.notes_main_item_textview);
                item.setId(i);
                item.setOnClickListener(ListOclBtn);
                //if (bV.get(i).length()>10)
                //tvName.setText(bV.get(i).substring(0,10));
                //else
                tvName.setText(titles.get(i));
                if (targetWas.get(i)==1)
                    tvName.setPaintFlags(tvName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                tvName.setTypeface(null,Typeface.NORMAL);
                item.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
                //item.setBackgroundColor(-1179716);
                linLayout.addView(item);
            }

            //Intent i = new Intent(getActivity(), AppMainActivity.class);
			/*Intent i = getBaseContext().getPackageManager()
				.getLaunchIntentForPackage( getBaseContext().getPackageName() );****
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | //Intent.FLAG_ACTIVITY_SINGLE_TOP);//
			Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);*/
        }
    }
	/*private void PFsaveFile(String FileName) {
		try {
			OutputStream outputstream = getActivity().openFileOutput(FileName+".txt", 0);
			OutputStreamWriter osw = new OutputStreamWriter(outputstream);
			for(int i=0;i<notes.size();i++)
				osw.write("com.koandr.organizer\n"+notes.get(i)+"\n");
			osw.close();
		} catch (Throwable t) {}
	}*/
}