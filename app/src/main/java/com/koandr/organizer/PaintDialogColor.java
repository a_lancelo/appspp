package com.koandr.organizer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

public class PaintDialogColor extends DialogFragment implements OnClickListener {
	
	public static PaintDialogColor newInstance(int num){

		PaintDialogColor dialogFragment = new PaintDialogColor();
	    Bundle bundle = new Bundle();
	    bundle.putInt("num", num);
	    dialogFragment.setArguments(bundle);

	    return dialogFragment;
	}
	
	int color=0;
	
	public Dialog onCreateDialog(Bundle savedInstanceState) {		
		
		//getDialog().setTitle(R.string.preferences);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View rootView = inflater.inflate(R.layout.paint_choosecolor, null);
		
		final ImageView colormap = (ImageView) rootView.findViewById(R.id.colormap);
		
		colormap.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				BitmapDrawable bd = (BitmapDrawable) colormap.getDrawable();
				Bitmap bitmap = bd.getBitmap();

				// get the color value. 
				// scale the touch location
				int x = (int) ((event.getX()-15) * bitmap.getWidth() / (colormap.getWidth()-30));
				int y = (int) ((event.getY()-15) * bitmap.getHeight() / (colormap.getHeight()-30));

				if (x >= bitmap.getWidth())
					x = (int) bitmap.getWidth() - 1;
				if (x < 0)
					x = 0;

				if (y >= bitmap.getHeight())
					y = (int) bitmap.getHeight() - 1;
				if (y < 0)
					y = 0;

				// set the color
				color = bitmap.getPixel(x, y);
				rootView.setBackgroundColor(color);

				return true;
			}
		});
		
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
        .setTitle(R.string.preferences)
        //.setMessage(R.string.about_app)
        .setView(rootView)
        .setNeutralButton(R.string.cancel, this)
		.setPositiveButton(R.string.ok, this)
		.setCancelable(false);
    return adb.create();
  }

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		Intent i = new Intent();
	    i.putExtra("color", color);
	    int INT_CODE = 1;
	    getTargetFragment().onActivityResult(getTargetRequestCode(), INT_CODE, i);
	    dismiss();
	}
}
