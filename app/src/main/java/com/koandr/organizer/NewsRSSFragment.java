package com.koandr.organizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.net.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import android.net.*;
import android.widget.*;
import android.view.*;
import android.view.View.OnTouchListener;
import android.app.Activity;
import android.content.*;
import android.os.*;
import android.support.v4.app.*;
import android.support.v4.widget.SwipeRefreshLayout;
import android.webkit.WebView;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class NewsRSSFragment extends Fragment {

	private ArrayList<NewsRSSCard> cards = new ArrayList<NewsRSSCard>();
	private Boolean loading = false;
	private int offset;
	private SwipeRefreshLayout swipeRefreshLayout; 
	private LinearLayout linearLayout;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getActivity().setTitle(R.string.news_rss);
        View rootView = inflater.inflate(R.layout.news_main, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
        
	        @Override
	        public void onRefresh() {
	        	if (!loading) {
	        		loadCards(getActivity().getSharedPreferences(AppConst.PreferencesFile, Activity.MODE_PRIVATE).getInt("newsPosition", -1));
	        	}
	        }
	        
		});
        swipeRefreshLayout.setColorScheme(android.R.color.white, android.R.color.white, android.R.color.white, android.R.color.white);
        ScrollView scrollView = (ScrollView) rootView.findViewById(R.id.scrollView); 
        scrollView.setOnTouchListener(new OnTouchListener() {
        	
        	@Override
            public boolean onTouch(View v, MotionEvent event) {
            	if (event.getAction() == MotionEvent.ACTION_UP & !loading) {
            		addNews(2);
            	}
                return false;
            }
            
        });
        linearLayout = (LinearLayout) rootView.findViewById(R.id.linearLayout);	
	    if (isFirstTime()) {
	    	setDefaultNews();
	    }
	    loadCards(getActivity().getSharedPreferences(AppConst.PreferencesFile, Activity.MODE_PRIVATE).getInt("newsPosition", -1));
		setHasOptionsMenu(true);
        return rootView;		
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.news_rss, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_item_add) {
			Intent intent = new Intent(getActivity(), AppMainActivity.class);
			intent.putExtra("screen", AppConst.NewsRSSBookmarks);
			startActivityForResult(intent, 1);
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		((AppMainActivity) getActivity()).onSelectItem(resultCode, data);
		if (resultCode == 1) {
        	if (!loading) {
        		loadCards(data.getExtras().getInt("position"));	
        	}
		}
	}
	
	private boolean isFirstTime() {
		SharedPreferences sharedPreferences = getActivity().getSharedPreferences(AppConst.PreferencesFile, Activity.MODE_PRIVATE); 
	    if (sharedPreferences.getBoolean("newsRSSFirstTimeV1", true)) {
	    	SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean("newsRSSFirstTimeV1", false);
			editor.commit();
			return true;
	    }
	    return false;
	}
	
	private void setDefaultNews() {
		ArrayList<NewsRSSBookmark> bookmarks = new ArrayList<NewsRSSBookmark>();
		NewsRSSBookmark bookmark = new NewsRSSBookmark();
		bookmark.setTitle(getString(R.string.title_loading));
		//if (!Locale.getDefault().getDisplayLanguage().equals("�������")) {
        if (!Locale.getDefault().getISO3Language().equals("rus")) {
			bookmark.setURL("http://feeds.bbci.co.uk/sport/0/rss.xml?edition=uk"); 
		} else {
			bookmark.setURL("http://news.sportbox.ru/taxonomy/term/7212/0/feed"); 
		}
		bookmarks.add(bookmark);
		new NewsRSSStorage().setBookmarks(getActivity(), bookmarks);
	}
	
	public boolean isOnline() {    
		ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);  
		NetworkInfo nInfo = cm.getActiveNetworkInfo();    
		if (nInfo != null && nInfo.isConnected()) {        
			return true;    
		}  
		return false;
	}
	
	@SuppressWarnings("deprecation")
	private void addNews(int step) {
		if (linearLayout == null) {
			return;
		}
		if (offset+step > cards.size()) {
			step = cards.size()-offset;
		}
		for (int i = offset; i < offset+step; i++) {
			View view = getActivity().getLayoutInflater().inflate(R.layout.news_main_item, null);
			((TextView) view.findViewById(R.id.siteTitle)).setText(cards.get(i).getSiteTitle());// feed.getItem(i).getNamePublisher());
			((TextView) view.findViewById(R.id.title)).setText(cards.get(i).getTitle());//feed.getItem(i).getTitle());
			WebView webView = (WebView) view.findViewById(R.id.news);
			webView.loadDataWithBaseURL("", cards.get(i).getText(), "text/html", "UTF-8", null);
			webView.getSettings().setDefaultFontSize(16);
			webView.setId(i);
			webView.setOnTouchListener(new View.OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_UP & isOnline()) {	
						Intent intent = new Intent(getActivity(), AppMainActivity.class);
						intent.putExtra("screen", AppConst.NewsRSSBrowser);
						intent.putExtra("url", cards.get(v.getId()).getURL());
						startActivityForResult(intent, 1);
					}
					return true;
				}
			});
			((TextView) view.findViewById(R.id.date)).setText(cards.get(i).getDate());
			view.setId(i);
			view.setOnTouchListener(new View.OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_UP & isOnline()) {		
						//Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(cards.get(v.getId()).getURL()));
						//startActivity(i);
						Intent intent = new Intent(getActivity(), AppMainActivity.class);
						intent.putExtra("screen", AppConst.NewsRSSBrowser);
						intent.putExtra("url", cards.get(v.getId()).getURL());
						startActivityForResult(intent, 1);
					}
					return true;
				}
			});
			linearLayout.addView(view);
		}
		offset = offset+step;
	}
	
	private void loadCards(int position) {
		ArrayList<NewsRSSBookmark> bookmarks = new NewsRSSStorage().getBookmarks(getActivity());
		switch (position) {
			case -1:
				new LoadRSSFeed(bookmarks).execute();
				break;
			default:
				ArrayList<NewsRSSBookmark> bookmark = new ArrayList<NewsRSSBookmark>();
				if (position >= bookmarks.size()) {
					position = bookmarks.size()-1;
				}
				if (position >= 0) {
					bookmark.add(bookmarks.get(position));
				}
				new LoadRSSFeed(bookmark).execute();
				break;
		}
	}
	
	private class LoadRSSFeed extends AsyncTask<Void, Void, Void> {
		
		private ArrayList<NewsRSSBookmark> bookmarks;
		private ArrayList<NewsRSSCard> cards;
		
		public LoadRSSFeed(ArrayList<NewsRSSBookmark> bookmarks) {
			this.bookmarks = bookmarks;
			//Toast.makeText(getActivity(), "bookSize: "+bookmarks.size(), 1).show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			//feed = new DOMParser().parseXML(bookmarks);
			cards = new ArrayList<NewsRSSCard>();

			/*NewsRSSCard card1 = new NewsRSSCard();
			//card1.setDate("");
			card1.setText("SSD");
			card1.setSiteTitle("Donut");
			card1.setURL("DDD");
			card1.setTitle("title");
			cards.add(card1);*/
			
			for (int i = 0; i < bookmarks.size(); i++) {			
				URL url = null;
				try {
					url = new URL(bookmarks.get(i).getURL());
				} catch (MalformedURLException e) {
					//e.printStackTrace();
				}					
					
				try {	
					
					DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document doc = builder.parse(new InputSource(url.openStream()));
					doc.getDocumentElement().normalize();
					NodeList list = doc.getElementsByTagName("item");
					if (list.getLength() == 0) {
						list = doc.getElementsByTagName("entry");
					}
					NodeList l = doc.getElementsByTagName("title");
					for (int j = 0; j < list.getLength(); j++) {
						Node currentNode = list.item(j);
						NewsRSSCard card = new NewsRSSCard();
						card.setSiteTitle(l.item(0).getFirstChild().getNodeValue());
						NodeList nodeChild = currentNode.getChildNodes();
						int cLength = nodeChild.getLength();
						for (int k = 0; k < cLength; k++) { 
							String nodeName = nodeChild.item(k).getNodeName(), nodeString = null;
							if(nodeChild.item(k).getFirstChild() != null){
								nodeString = nodeChild.item(k).getFirstChild().getNodeValue();
							} else if (nodeName.equals("link")){
								nodeString = nodeChild.item(k).getAttributes().getNamedItem("href").getNodeValue();
							}
							if (nodeString != null) {
								if ("title".equals(nodeName)) {
									card.setTitle(nodeString);
								} else if ("description".equals(nodeName)||"content".equals(nodeName)) {
									card.setText(nodeString);
								} else if ("pubDate".equals(nodeName)||"published".equals(nodeName)) {
									card.setDate(nodeString);
								} else if ("link".equals(nodeName)) {
									card.setURL(nodeString);
								} 
							}
						}
						if (j == 0) {
							ArrayList<NewsRSSBookmark> bookmarks = new NewsRSSStorage().getBookmarks(getActivity());
							for (int k = 0; k < bookmarks.size(); k++) {
								if (bookmarks.get(k).getURL().equals(this.bookmarks.get(i).getURL())) {
									bookmarks.get(k).setTitle(card.getSiteTitle());
									break;
								}
							}
							new NewsRSSStorage().setBookmarks(getActivity(), bookmarks);
						}
						cards.add(card);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			Collections.sort(cards, new Comparator<NewsRSSCard>() {
					public int compare(NewsRSSCard m2, NewsRSSCard m1) {
						return m1.getDateFormated().compareTo(m2.getDateFormated());
					}
				});
			if (isCancelled()) return null;
			return null;
		}

		@Override
		protected void onPreExecute() {
            swipeRefreshLayout.setRefreshing(true);
			loading = true;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			loading = false;
            swipeRefreshLayout.setRefreshing(false);
			//Toast.makeText(getActivity(), "cardSize: "+cards.size(), 1).show();
			//Toast.makeText(getActivity(), "BcardSize: "+NewsRSSFragment.this.cards.size(), 1).show();
			if (cards.size() > 0) {
				new NewsRSSStorage().setCardsCash(getActivity(), cards);
			} else {
				if (NewsRSSFragment.this.cards.size() > 0) {
					return;
				}
				cards = new NewsRSSStorage().getCardsCash(getActivity());
			}
			linearLayout.removeAllViews();
			NewsRSSFragment.this.cards = cards;
			offset = 0;
			addNews(10);
		}	
		
	}
	
}
