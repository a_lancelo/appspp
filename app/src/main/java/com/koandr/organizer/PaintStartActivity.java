package com.koandr.organizer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;


public class PaintStartActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.app_some_activity_main);
		FragmentManager fragmentManager = getSupportFragmentManager();
		
		//FragmentManager fm = getFragmentManager();
		//fragmentManager.add
		fragmentManager.addOnBackStackChangedListener(new FragmentManager. OnBackStackChangedListener() {
				@Override
				public void onBackStackChanged() {
					if(getSupportFragmentManager().getBackStackEntryCount() == 0) finish();
					
				/*	getSupportFragmentManager().
						beginTransaction().
						add(R.id.fragment, fragment).
						addToBackStack(null).
						commit();****
					//getSupportFragmentManager().popBackStack();*/
					
				}
			});
			
		final Fragment fragment = new PaintFragment();
		if (fragment!=null)
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment). addToBackStack(null).commit(); 
		
			
	}
	
	
	/*public void onBackPressed(View v) {
		getSupportFragmentManager().popBackStack();
		
	}*/

}