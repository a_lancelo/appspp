package com.koandr.organizer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class FileManagerRemoverDialog extends DialogFragment {

	public Dialog onCreateDialog(Bundle savedInstanceState) {				
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
			.setTitle(R.string.delete_this)			
			.setNeutralButton(R.string.cancel, listener)
			.setPositiveButton(R.string.ok, listener);
		return adb.create();
	}

	DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int id) {						
			if (id == -1) {
				getTargetFragment().onActivityResult(getTargetRequestCode(), id, null);
			}		
			dismiss();       
		}

    };
	
}
