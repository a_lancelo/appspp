package com.koandr.organizer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.*;

public class FileManagerAdderDialog extends DialogFragment {

	RadioGroup radioGroup;
	EditText name;

	public Dialog onCreateDialog(Bundle savedInstanceState) {				
		View rootView = getActivity().getLayoutInflater().inflate(R.layout.file_manager_adder_dialog, null);//, false);
		radioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup);
		name = (EditText) rootView.findViewById(R.id.name);		
		radioGroup.check(R.id.folder);		
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
			.setTitle(R.string.create_new)
			.setView(rootView)
			.setNeutralButton(R.string.back, listener)
			.setPositiveButton(R.string.ok, listener);
		return adb.create();
	}
	
	DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int id) {						
			if (id == -1) {
				Intent data = new Intent();
				data.putExtra("name", name.getText().toString());
				int type = 0;
				switch(radioGroup.getCheckedRadioButtonId()) {
					case R.id.file:
						type = R.id.file;
						break;
					case R.id.folder:
						type = R.id.folder;
						break;
				}												
				data.putExtra("type", type);
				getTargetFragment().onActivityResult(getTargetRequestCode(), 1, data);
			}		
			dismiss();       
		}
		
    };
	
}
