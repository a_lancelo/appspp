package com.koandr.organizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
//import android.view.View.OnClickListener;
import android.widget.SeekBar;

public class PaintDialogPreferences extends DialogFragment implements OnClickListener, android.content.DialogInterface.OnClickListener {

	public static PaintDialogPreferences newInstance(int num){

		PaintDialogPreferences dialogFragment = new PaintDialogPreferences();
	    Bundle bundle = new Bundle();
	    bundle.putInt("num", num);
	    dialogFragment.setArguments(bundle);

	    return dialogFragment;
  }
	
	int burch, bground, seekbar;//, cancel;
	SeekBar seekBar;
	//String event;
	
	public Dialog onCreateDialog(Bundle savedInstanceState) {		
		
		//getDialog().setTitle(R.string.preferences);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View rootView = inflater.inflate(R.layout.paint_preferences, null);//, false);
		
		/*sB = (SeekBar) cV.findViewById(R.id.seekBar);
		sB.setMax(100);
		sB.setProgress(s);
		sB.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					s=sB.getProgress();
					savePreferences();
					return false;
				}
			});*/
		rootView.findViewById(R.id.paint_preferences_burch).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*DialogFragment dialogFrag = PaintDialogColor.newInstance(123);
	            dialogFrag.setTargetFragment(PaintDialogPreferences.this, 1);
	            
	            //(this, DIALOG_FRAGMENT);
	            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");*/
				returnData("burch");
			}
		});
		rootView.findViewById(R.id.paint_preferences_background).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*DialogFragment dialogFrag = PaintDialogColor.newInstance(123);
	            dialogFrag.setTargetFragment(PaintDialogPreferences.this, 2);
	            
	            //(this, DIALOG_FRAGMENT);
	            dialogFrag.show(getFragmentManager().beginTransaction(), "dialog");*/
				returnData("bground");
			}
		});
		
		rootView.findViewById(R.id.paint_preferences_open).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
				returnData("open");
			}
		});
		rootView.findViewById(R.id.paint_preferences_save).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				returnData("save");
				//getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
			}
		});
		/*rootView.findViewById(R.id.paint_preferences_cancel).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cancel++;
				//getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
			}
		});*/
		rootView.findViewById(R.id.paint_preferences_clear).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				returnData("clear");
				//getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
			}
		});
		
		
		rootView.findViewById(R.id.paint_preferences_seekbar).setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				//s=sB.getProgress();
				//savePreferences();
				seekbar=seekBar.getProgress();
				return false;
			}
		});
		
		
		
			String MY_PREF = "MY_PREF";
			int mode = Activity.MODE_PRIVATE;
			SharedPreferences mySharedPreferences = getActivity().getSharedPreferences(MY_PREF, mode); 
			rootView.findViewById(R.id.paint_preferences_seekbar);		
			//SeekBar 
			seekBar = (SeekBar) rootView.findViewById(R.id.paint_preferences_seekbar);
			seekBar.setMax(100);
			seekBar.setProgress(mySharedPreferences.getInt("PaintSizeBurch", 15));
		
		/*rootView.findViewById(R.id.paint_preferences_back).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
				dismiss();
			}
		});*/
		
		///return rootView;
	//}
	
	 /* public void onDismiss(DialogInterface dialog) {
		    super.onDismiss(dialog);
		  }

	  public void onCancel(DialogInterface dialog) {
		    super.onCancel(dialog);
		  }
	  */
	
	  
			//final String[] Encoding = {"Windows-1251","UTF-8"};
			  		  
		    AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
		        .setTitle(R.string.preferences)
		        //.setMessage(R.string.about_app)
		        .setView(rootView)
		        .setNeutralButton(R.string.back, this);
		    return adb.create();
		  }

	
		public void returnData(String event){
			Intent i = new Intent();
		    //i.putExtra("burch", burch);
		    //i.putExtra("bground", bground);
		    i.putExtra("seekbar", seekbar);
		    //i.putExtra("cancel", cancel);
		    i.putExtra("event", event);
		    int INT_CODE = 1;
		    getTargetFragment().onActivityResult(getTargetRequestCode(), INT_CODE, i);
		    dismiss();
		}

		  public void onDismiss(DialogInterface dialog) {
		    super.onDismiss(dialog);
		    //Log.d(LOG_TAG, "Dialog 2: onDismiss");
		  }

		  public void onCancel(DialogInterface dialog) {
		    super.onCancel(dialog);
		    //Log.d(LOG_TAG, "Dialog 2: onCancel");
		    returnData("back");
		  }

		@Override
		public void onClick(DialogInterface arg0, int arg1) {
			returnData("back");
			// TODO Auto-generated method stub
			//int i = 0;
		    /*switch (arg1) {
		    case Dialog.BUTTON_POSITIVE:
		      //i = R.string.yes;
		      getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
		      break;
		    case Dialog.BUTTON_NEGATIVE:
		    	getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, getActivity().getIntent());
		     // i = R.string.no;
		      break;
		    case Dialog.BUTTON_NEUTRAL:
		     // i = R.string.maybe;
		      break;
		    }*/
		    //if (i > 0)
		      //Log.d(LOG_TAG, "Dialog 2: " + getResources().getString(i));
		}

		
		/*@Override
		public void onActivityResult(int requestCode, int resultCode, Intent data) {
		    if (requestCode == 1){
		    	if (data.getIntExtra("color", 0)!=0)
		    	burch = data.getIntExtra("color", 0);
		    }
		    if (requestCode == 2){
		    	if (data.getIntExtra("color", 0)!=0)
		    	bground = data.getIntExtra("color", 0);
		    }
		    
		}*/
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
		}
}
