package com.koandr.organizer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;

public class AppStartActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.app_some_activity_main);
		startFragment(getIntent().getIntExtra("number", 0));
	}
	
	public void startFragment(int number) {

		Fragment fragment = null;
		
		switch(number) {
			case 0:
				fragment = new PaintFragment();
				break;
			case 1:
				fragment = new MusicPlayerExtensionFragment();
				break;
		}
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		if (fragment!=null)
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit(); 
	}
	
}
