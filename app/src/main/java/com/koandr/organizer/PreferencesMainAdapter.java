package com.koandr.organizer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PreferencesMainAdapter extends BaseAdapter {
	
	Context context;
	String [] titles, summaries;
	
	PreferencesMainAdapter(Context context, String [] titles, String [] summaries) {
       this.context = context;
       this.titles = titles;
       this.summaries = summaries;
    }
	
	@Override
	public int getCount() {
	   return titles.length;
	}
	
	@Override
	public Object getItem(int position) {
	   return null;
	}
	
	@Override
	public long getItemId(int position) {
	   return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	   View view = (View) ((AppMainActivity) context).getLayoutInflater().inflate(R.layout.preferences_main_text_view, parent, false);
	   ((TextView) view.findViewById(R.id.title)).setText(titles[position]);
	   ((TextView) view.findViewById(R.id.summary)).setText(summaries[position]);
	   return view;
	}
	
}
