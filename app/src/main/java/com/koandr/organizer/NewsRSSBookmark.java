package com.koandr.organizer;

public class NewsRSSBookmark {
	
	private String title, URL;
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}
	
	public String getTitle() {
		return title;
	}

	public String getURL() {
		return URL;
	}
	
}
