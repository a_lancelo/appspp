package com.koandr.organizer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class AppSidebarMenuAdapter extends BaseAdapter {
	
	Context context;
	String [] titles;
	int [] images;
	
    AppSidebarMenuAdapter(Context context, String [] titles, int [] images) {
       this.context = context;
       this.titles = titles;
       this.images = images;
    }
	
	@Override
	public int getCount() {
	   return titles.length;
	}
	
	@Override
	public Object getItem(int position) {
	   return null;
	}
	
	@Override
	public long getItemId(int position) {
	   return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	   View view = (View) ((AppMainActivity) context).getLayoutInflater().inflate(R.layout.app_slidingmenu_item, parent, false);
	   ImageView imageView = (ImageView) view.findViewById(R.id.app_slidingmenu_item_imageview);
	   imageView.setImageResource(images[position]);
	   TextView textView = (TextView) view.findViewById(R.id.app_slidingmenu_item_textview);
	   textView.setText(titles[position]);
	   //if (position!=getCount()-1){
	     //	View selector = ((AppMainActivity) context).getLayoutInflater().inflate(R.layout.app_slidingmenu_selector, view, false);
	     //	view.addView(selector);
	   //}
	   return view;
	}

}
