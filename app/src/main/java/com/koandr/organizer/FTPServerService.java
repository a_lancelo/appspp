package com.koandr.organizer;

import android.app.*;
import android.content.*;
import android.os.*;
import android.net.wifi.*;
import android.text.format.Formatter;
import android.net.*;

public class FTPServerService extends Service { 

	private static final int NOTIFICATION_ID = 3;

	public class LocalBinder extends Binder {
		FTPServerService getService() {
			return FTPServerService.this;
		}
	}

	private FTPServer ftpServer = null;
	private LocalBinder m_binder = new LocalBinder();
	private NotificationManager m_notificationMgr;
	private Notification m_notification;

    private final long mFrequency = 100;
    private final int TICK_WHAT = 2; 
	private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
        	updateNotification();
        	sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

	@Override
	public IBinder onBind(Intent intent) {
		return m_binder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		m_notificationMgr = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		createNotification();
	}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public void createNotification() {
    	int icon = R.drawable.slidingmenu_item_7;
    	CharSequence tickerText = "Apps++";
    	long when = System.currentTimeMillis();
    	m_notification = new Notification(icon, tickerText, when);
    	m_notification.flags |= Notification.FLAG_ONGOING_EVENT;
    	m_notification.flags |= Notification.FLAG_NO_CLEAR;
    }

    public void updateNotification() {
    	Context context = getApplicationContext();		
		WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		String ip = "ftp://" + Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress()) + ":2020";
		CharSequence contentTitle = ip;
    	CharSequence contentText = "Apps++ " + getString(R.string.ftp_server);
    	Intent notificationIntent = new Intent(this, MainActivity.class);
    	notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    	PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,// 0);
																PendingIntent.FLAG_CANCEL_CURRENT);
    	m_notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
    	m_notificationMgr.notify(NOTIFICATION_ID, m_notification);
    }

	public void showNotification() {
    	updateNotification();    	
        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
    }

    public void hideNotification() {
    	m_notificationMgr.cancel(NOTIFICATION_ID);
    	mHandler.removeMessages(TICK_WHAT);
    }

	public void start() {
		if (ftpServer == null) {
			ftpServer = new FTPServer();
			ftpServer.start();
			takeWakeLock();					
			showNotification();
		}
	}

	public void pause() {
		ftpServer.interrupt();
		ftpServer = null;
		hideNotification();		
		if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }		
	}

	public boolean isRunning() {
		if (ftpServer != null) { 
			return true;
		} else {
			return false;
		}
	}
	
	private PowerManager.WakeLock wakeLock = null;
	
	private void takeWakeLock() {
        if (wakeLock == null) {
            PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "TAG");          
            wakeLock.setReferenceCounted(false);
        }
       	wakeLock.acquire();
    }
	
	public boolean isOnline() {
		ConnectivityManager cm =
			(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	
}

