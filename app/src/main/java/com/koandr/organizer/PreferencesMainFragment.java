package com.koandr.organizer;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;


public class PreferencesMainFragment extends Fragment {
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	getActivity().setTitle(R.string.preferences);
        View rootView = (View) inflater.inflate(R.layout.preferences_main, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.listView);
		String [] titles = { getString(R.string.rate), getString(R.string.feedback), getString(R.string.about_apps) };
		String [] summaries = { getString(R.string.google_play), getString(R.string.feedback_address), null };
		listView.setAdapter(new PreferencesMainAdapter(getActivity(), titles, summaries));
		listView.setOnItemClickListener(itemClickListener);
        return rootView;
    }
	
	OnItemClickListener itemClickListener = new OnItemClickListener() {
		
		@Override
		public void onItemClick(AdapterView<?> parent, View v,
								int position, long id) {
			switch(position) {
				case 0:
					Intent intent=new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse("market://details?id=com.koandr.organizer")); 
					startActivity(intent);
					break;
				case 1:
					Uri uriUrl = Uri.parse("https://vk.com/public67996546"); 
					Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl); 
					startActivity(launchBrowser);
					break;
			}
		}
		
	};

}
