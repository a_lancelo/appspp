package com.koandr.organizer;

import java.util.ArrayList;
import java.util.List;

import android.app.*;
import android.content.*;
import android.database.*;
import android.os.*;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.*;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.*;
import android.support.v4.app.Fragment;

public class MusicPlayerMainFragment extends Fragment {
	
	List<String> data = new ArrayList<String>();
	List<String> names = new ArrayList<String>();
	List<String> artists = new ArrayList<String>();
	AppScrollTextView name, artist;
	Button play;
	int position, progress;
	public static String MY_PREF = "MY_PREF";

    SeekBar seekBar;
    TextView duration, current;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		getActivity().setTitle(R.string.music);
        getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);
		loadPreferences();
        View rootView = inflater.inflate(R.layout.music_player_main, container, false);
        ListView lv = (ListView) rootView.findViewById(R.id.listView);
        play = (Button) rootView.findViewById(R.id.play);
		name = (AppScrollTextView) rootView.findViewById(R.id.name);
		artist = (AppScrollTextView) rootView.findViewById(R.id.artist);
        //name.setSelected(true);
        //artist.setSelected(true);

        duration= (TextView) rootView.findViewById(R.id.duration);
        current= (TextView) rootView.findViewById(R.id.current);
        seekBar = (SeekBar) rootView.findViewById(R.id.seekBar);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (data.size() != 0) {
                    if (!musicService.isStopwatchRunning()) {
                        musicService.start();
                    }
                    //musicService.setProgress(seekBar.getProgress());
                    musicService.setSeekBarProgress(seekBar.getProgress());
                    play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_pause));
                }
                return false;
            }
        });

		rootView.findViewById(R.id.play).setOnClickListener(new OnClickListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				if (data.size()!=0) {
					if (musicService.isStopwatchRunning()) {		
						position = musicService.getPosition();
						progress = (int) musicService.getStateDuration();
						musicService.pause();
						savePreferences();
						play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_play));
					} else {
						if (position >= data.size()) position = data.size()-1;
						musicService.setPosition(position);
						musicService.setProgress(progress);
						musicService.start();
						play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_pause));
					}
				}
			}
		});
		
		rootView.findViewById(R.id.currentSongBar).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*Intent intent = new Intent(getActivity(), AppStartActivity.class);
				intent.putExtra("number", 1);
				startActivity(intent);*/
                View view = getView().findViewById(R.id.moreInfo);
                if (view.getVisibility() == View.VISIBLE) {
                    view.setVisibility(View.GONE);
                } else {
                    view.setVisibility(View.VISIBLE);
                }
			}
		});
        rootView.findViewById(R.id.moreInfo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getView().findViewById(R.id.moreInfo).setVisibility(View.GONE);
            }
        });
        rootView.findViewById(R.id.prev).setOnClickListener(new OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                musicService.previousSing();
                play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_pause));
            }
        });
        rootView.findViewById(R.id.next).setOnClickListener(new OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                musicService.nextSing();
                play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_pause));
            }
        });

        BindAllSongs();
		lv.setAdapter(new MusicPlayerMainAdapter(getActivity(), names.toArray(new String[names.size()]), artists.toArray(new String[artists.size()])));			
		lv.setOnItemClickListener(itemListener);
		if (data.size() != 0) {
			if (position >= data.size()) position = data.size()-1;
			name.setText(names.get(position));	
			artist.setText(artists.get(position));
			getActivity().getApplication().startService(new Intent(getActivity(), MusicPlayerService.class));
			bindStopwatchService();		
			mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);			
		} else {
            rootView.findViewById(R.id.empty).setVisibility(View.VISIBLE);
        }
		return rootView;
	}		
	
	OnItemClickListener itemListener = new OnItemClickListener() {
		@SuppressWarnings("deprecation")
		@Override
		public void onItemClick(AdapterView<?> parent, View v,
								int position, long id) { 				
			if (musicService!=null) musicService.reset();				
			musicService.setPosition(position);
			musicService.start();	
			play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_pause));
		}
	};
	
	public void BindAllSongs() {
		String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
		final String[] projection = new String[] {
			MediaStore.Audio.Media.DISPLAY_NAME,
			MediaStore.Audio.Media.ARTIST,
			MediaStore.Audio.Media.DATA};
		final String sortOrder = MediaStore.Audio.AudioColumns.TITLE
			+ " COLLATE LOCALIZED ASC";

		Cursor cursor = null;
		try {
			Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
			cursor = getActivity().getContentResolver().query(uri, projection, selection, null, sortOrder);
			if (cursor != null) {
				cursor.moveToFirst();                       
				while(!cursor.isAfterLast()) { 
					data.add(cursor.getString(2));
					names.add(cursor.getString(0).substring(
					0,cursor.getString(0).length()-4));
					artists.add(cursor.getString(1));
					cursor.moveToNext();
				}
			}
		} catch (Exception ex) {

		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}       

	}

	@Override
	public void onResume(){
		super.onResume();
	}
	
    private final long mFrequency = 100; // milliseconds
    private final int TICK_WHAT = 2; 
	private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
        	updateElapsedTime();
        	sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

    // Connection to the backgorund StopwatchService
	private MusicPlayerService musicService;
	private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			musicService = ((MusicPlayerService.LocalBinder)service).getService();
			showCorrectButtons();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			musicService = null;
		}
	};
	
	private void bindStopwatchService() {
		getActivity().getApplication().bindService(new Intent(getActivity(), MusicPlayerService.class), 
        			serviceConnection, Context.BIND_AUTO_CREATE);
	}
	
	private void unbindStopwatchService() {
		if (musicService != null) {
			getActivity().getApplication().unbindService(serviceConnection);
		}
	}
	
    @Override
	public void onDestroy() {
        super.onDestroy();
        unbindStopwatchService();
    }
    
    @SuppressWarnings("deprecation")
	private void showCorrectButtons() {
    	if (musicService != null) {
    		musicService.setListSong(data);
    		musicService.setListArtists(artists);
			//recoverySing();
    		if (musicService != null && musicService.isStopwatchRunning()) {
    			play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_pause));
    		}
    	}
    }
    
    public void updateElapsedTime() {
		if (musicService != null && musicService.isStopwatchRunning()) { /*	//! getFormattedElapsedTime().equals("00:00")){
			if (!play.getBackground().equals(getResources().getDrawable(R.drawable.ic3))) {
				play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic3));
			}*/
			progress = (int) musicService.getStateDuration();
			name.setText(musicService.getSongName());
			artist.setText(musicService.getArtist());


            current.setText(musicService.getFormattedElapsedTime());
            duration.setText(musicService.getDuration());
            seekBar.setMax(musicService.getIntDuration());
            seekBar.setProgress((int) musicService.getStateDuration());
		}
    }
    
	public void loadPreferences() {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getActivity().getSharedPreferences(MY_PREF, mode); 
		position = mySharedPreferences.getInt("music_player_position", 0);
		progress = mySharedPreferences.getInt("music_player_progress", 0);
	}

	protected void savePreferences() {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getActivity().getSharedPreferences(MY_PREF, mode); 	
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("music_player_position", position);
		editor.putInt("music_player_progress", progress);
		editor.commit();
	}
	
	/*@SuppressWarnings("deprecation")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) { 
		loadPreferences();
		if (position >= data.size()) position = data.size()-1;
		name.setText(names.get(position));	
		artist.setText(artists.get(position));
		if (musicService != null && musicService.isStopwatchRunning()) {
			play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_pause));
            /*current.setText(musicService.getFormattedElapsedTime());
            duration.setText(musicService.getDuration());
            seekBar.setMax(musicService.getIntDuration());
            seekBar.setProgress((int) musicService.getStateDuration());****
		} else {
			play.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_play));
		}
	}*/
	
}
